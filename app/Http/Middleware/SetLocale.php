<?php

namespace App\Http\Middleware;

use Closure;

class SetLocale
{
    public function handle($request, Closure $next)
    {
       
        if (\Session::has('locale')) {
            app()->setLocale(\session::get('locale'));
        }

        return $next($request);
    }
}
