<?php

namespace App\Http\Controllers\Admin;
use App\Requestwork;
use App\project;
class HomeController
{
    public function index()
    {
        return view('home',
         [   'request' => Requestwork::count(),
            'projects' => project::where('type', 1)->count(),
            'project' => project::where('type', 2)->count(),
        ]);
    }
}
