<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\about;
use App\Requestwork;
use Image;
use App\project;
use App\settings;
use App\service;
use App\contactin;
use App\video;
class backendadmin extends Controller
{
    	public function show($id)
	{
		    	$about=about::find($id);
		return view('admin.about',compact('about'));
    }
    public function all()
{
    $about=about::all();
    return view('admin.something',compact('about'));
}
    public function about($id,Request $request)
    {
    	$about=about::find($id);
    	$about->headingar=$request->headingar;
    	$about->headingen=$request->headingen;
    	$about->contentar=$request->contentar;
    	$about->contenten=$request->contenten;
$about->save();
return redirect('admin/all')->with('success','success edit');
    }

public function showRequest()
    {
        $request=Requestwork::OrderBy('ID','ASC')->paginate(9);
        return view('admin.showRequest',compact('request'));
    }
    

    public function Details()
    {
    	$n=project::all();
        return view('admin.project.index',compact('n'));
    }

   public function shownew($id)
    {
    	$projects=project::find($id);
        return view('admin.project.show',compact('projects'));
    }
    public function create()
    {
    return view('admin.project.project');	
    }
    public function sa(Request $request)
    {
    
        $project=new project();
        $project->headingen=$request->headingen;
        $project->headingar=$request->headingar;
        $project->descriptionen=$request->descriptionen;
        $project->descriptionar=$request->descriptionar;

        if($request->hasFile('imageen')){
          $imgs=$request->imageen;
    $filenames=time() ."_".$imgs->getClientOriginalName();
    $localtions=public_path('projecten/'.$filenames);
      //$img->move($localtion);
      image::make($imgs)->resize(1000,1000)->save($localtions);
     
      }
             $project->imageen=$filenames;

        if($request->hasFile('imagear')){
          $img=$request->imagear;
    $filename=time() ."_".$img->getClientOriginalName();
    $localtion=public_path('projectar/'.$filename);
      //$img->move($localtion);
      image::make($img)->resize(1000,1000)->save($localtion);
     
      }     
  
        $project->imagear=$filename; 
        $project->number=$request->number;
        $project->type=$request->type;
        
        $project->map=$request->map;
        $project->meta_description=$request->meta_description;
        $project->meta_keywords=$request->meta_keywords;
             $project->save();
             
             return redirect('admin/Details')->with('success','success add');
             ;
                }
                public function setting()
                {
                    $setting=settings::all();;
                    return view('admin.settings.setting',compact('setting'));
                }
                public function settingDetails($id)
                {
                    $settingDetails=settings::find($id);
                    return view('admin.settings.settingDetails',compact('settingDetails'));
                }
                public function settingstro($id,Request $request)
                {
                    $settingstro=settings::find($id);
                    $settingstro->key=$request->name;
                    $settingstro->value=$request->content;
                    $settingstro->save();
                    return redirect('admin/setting')->with('success','success edit');;
                }
                
            
    public function destroy($id)
    {
        $product=project::find($id);
        $product->delete();

        return back();
    }
                
    public function editproject($id)
    {
        $project=project::find($id);
      

        return view('admin.project.edit',compact('project'));
    }
    public function Updateproject($id,Request $request)
    {
        $project=project::find($id);
        $project->headingen=$request->headingen;
        $project->headingar=$request->headingar;
        $project->descriptionen=$request->descriptionen;
        $project->descriptionar=$request->descriptionar;

        if($request->file('imageen')  !== null){
                  $img=$request->imageen;
            
            $filename=time() ."_".$img->getClientOriginalName();
            $localtion=public_path('projecten/'.$filename);
            
              //$img->move($localtion);
              image::make($img)->resize(800,400)->save($localtion);
             $project->imageen=$filename;
              }
              else{
            
                $project->imageen=$project->imageen;
              } 
              
        if($request->file('imagear')  !== null){
                  $img=$request->imagear;
            
            $filename=time() ."_".$img->getClientOriginalName();
            $localtion=public_path('projectar/'.$filename);
            
              //$img->move($localtion);
              image::make($img)->resize(800,400)->save($localtion);
             $project->imagear=$filename;
              }
              else{
            
                $project->imagear=$project->imagear;
              } 
        $project->number=$request->number;
        $project->type=$request->type;

        $project->map=$request->map;
        $project->meta_description=$request->meta_description;
        $project->meta_keywords=$request->meta_keywords;
             $project->save();
       

        return  redirect('admin/Details')->with('success','success edit');;
    }
     public function service(){
        $service=service::OrderBy('id','ASC')->paginate(9);

        return view('admin.service.index',compact("service"));
     }
     public function serviceedit($id){
        $service=service::find($id);

        return view('admin.service.edit',compact("service"));
     }
     public function servicesave($id,Request $request){
        $service=service::find($id);
        $service->headingen=$request->headingen;
        $service->headingar=$request->headingar;
        $service->descriptionen=$request->descriptionen;
        $service->descriptionar=$request->descriptionar;
        if($request->file('image')  !== null){
            // unlink('service/'.$service->image);
                  $img=$request->image;
            
            $filename=time() ."_".$img->getClientOriginalName();
            $localtion=public_path('service/'.$filename);
            
              //$img->move($localtion);
              image::make($img)->resize(600,300)->save($localtion);
             $service->image=$filename;
              }
              $service->save();
        return redirect('admin/service')->with('success','success edit');
     }
     public function contact(){
        $contactin=contactin::aLL();;
        return view('admin.contacts',compact('contactin'));
     }      public function video(){
return view('admin.video');
     }
      
     public function uploadvideo(Request $request,$id)
     {
        $video=video::find($id);

       
         if ($request->hasFile('video')) {
            $image = $request->file('video');
                        $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('uploadsvideo/');
            $image->move($destinationPath, $name);
            $video->video=$name;
     }
     $video->save();

     return back();

    }
}
