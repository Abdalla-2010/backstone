<?php

namespace App\Http\Controllers;
use Spatie\TranslationLoader\LanguageLine;
	
use Illuminate\Http\Request;
use App\about;
use App\Requestwork;
use App\project;
use App\settings;
use App\service;
use Illuminate\Support\Facades\Mail;
use App\Mail\Contact;
use App\contactin;
use App\video;
class Front extends Controller
{
	//  home page
    public function index()
    {
   $projects=project::orderBy('id','DESC')->where('type','2')->limit(2)->get();
$Facebook=settings::find('1');
$instagram=settings::find('2');
$youtube=settings::find('3');
$phoneOne=settings::find('4');
$phoneTwo=settings::find('5');
$Mail=settings::find('6');;
$Adress=about::find('2');
$deal=about::find('3');
$slogan=about::find('4');
   $projectspri=project::orderBy('id','DESC')->where('type','1')->limit(2)->get();
$service1=service::find('1');
$video=video::find(1);

    	return view('front.index',compact('video','projectspri','projects','Facebook','instagram','youtube','phoneOne','phoneTwo','Mail','Adress','deal','slogan','service1'));
    }
       public function about()
    {
        $Facebook=settings::find('1');
        $instagram=settings::find('2');
        $youtube=settings::find('3');
        $phoneOne=settings::find('4');
        $phoneTwo=settings::find('5');
        $Mail=settings::find('6');;
        $Adress=about::find('2');
        $deal=about::find('3');
        $slogan=about::find('4');
    $about=about::find('1');
    	return view('front.about',compact('about','Facebook','instagram','youtube','phoneOne','phoneTwo','Mail','Adress','deal','slogan'));
    } 
    public function Requestin(Request $request)
    { 
        $validatedData = $request->validate([
        'name' => 'required',
        'Email' => 'required',
         'number' => 'required',
 'buyarea' => 'required',
 'area' => 'required',
    ]);
 $rq=new Requestwork();
         $rq->name=$request->name;
         $rq->Email=$request->Email;
         $rq->number=$request->number;
         $rq->buyarea=$request->buyarea;
         $rq->area=$request->area;
         $rq->save();
           return response($rq); 

    }
public function Contact()
{
    
    $Facebook=settings::find('1');
    $instagram=settings::find('2');
    $youtube=settings::find('3');
    $phoneOne=settings::find('4');
    $phoneTwo=settings::find('5');
    $Mail=settings::find('6');;
    $Adress=about::find('2');
    $deal=about::find('3');
    $slogan=about::find('4');
    return view('front.contact',compact('Facebook','instagram','youtube','phoneOne','phoneTwo','Mail','Adress','deal','slogan'));
}
public function projects()
{

    $Facebook=settings::find('1');
    $instagram=settings::find('2');
    $youtube=settings::find('3');
    $phoneOne=settings::find('4');
    $phoneTwo=settings::find('5');
    $Mail=settings::find('6');;
    $Adress=about::find('2');
    $deal=about::find('3');
    $slogan=about::find('4');
    $projects=project::where('type','1')->orderBy('id','ASC')->paginate(4);
    $wordprojects=about::find('5');
    
    return view('front.work',compact('wordprojects','projects','Facebook','instagram','youtube','phoneOne','phoneTwo','Mail','Adress','deal','slogan'));
}
public function Previous_project()
{

    $Facebook=settings::find('1');
    $instagram=settings::find('2');
    $youtube=settings::find('3');
    $phoneOne=settings::find('4');
    $phoneTwo=settings::find('5');
    $Mail=settings::find('6');;
    $Adress=about::find('2');
    $deal=about::find('3');
    $slogan=about::find('4');
    $projects=project::where('type','2')->orderBy('id','ASC')->paginate(4);
    $wordprojectspro=about::find('6');

    return view('front.pri',compact('wordprojectspro','projects','Facebook','instagram','youtube','phoneOne','phoneTwo','Mail','Adress','deal','slogan'));
}

public function Details($id)
{
    $Facebook=settings::find('1');
    $instagram=settings::find('2');
    $youtube=settings::find('3');
    $phoneOne=settings::find('4');
    $phoneTwo=settings::find('5');
    $Mail=settings::find('6');;
    $Adress=about::find('2');
    $deal=about::find('3');

   $project=project::find($id);
    return view('front.Details',compact('project','Facebook','instagram','youtube','phoneOne','phoneTwo','Mail','Adress','deal'));
}
public function services()
{

    $Facebook=settings::find('1');
    $instagram=settings::find('2');
    $youtube=settings::find('3');
    $phoneOne=settings::find('4');
    $phoneTwo=settings::find('5');
    $Mail=settings::find('6');;
    $Adress=about::find('2');
    $deal=about::find('3');
    $slogan=about::find('4');
$service1=service::find('1');
$service2=service::find('2');
$service3=service::find('3');
    return view('front.services',compact('service1','service2','service3','Facebook','instagram','youtube','phoneOne','phoneTwo','Mail','Adress','deal','slogan'));
}
public function sendmall(Request $request){
    $request->validate([
        'name'=>'required',
        'number'=>'required',
     'Email'=>'required|Email',
  'subject'=>'required']);
  $contact=new contactin();
  $contact->name=$request->input('name');
  $contact->number=$request->input('number');
  $contact->Email=$request->input('Email');
  $contact->massage=$request->input('subject');
  $contact->save();
  
 $name=$request->input('name');
 $number=$request->input('number'); 
  $Email=$request->input('Email');      
    $subject=$request->input('subject');
mail::to("mohamedabdhiamed2@gamil.com")->send(new Contact($name,$number,$Email,$subject))->from($Email);
return back();
} 

}
