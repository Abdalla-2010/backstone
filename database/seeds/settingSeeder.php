<?php

use Illuminate\Database\Seeder;
use App\settings;
class settingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [[
            'id'             => 1,
            'value'           => '',
            'key'          => 'Facebook',
         
          
        ],
        [
            'id'             => 2,
            'value'           => '',
            'key'          => 'instagram',
      
          
        ],
        [
            'id'             => 3,
            'value'           => '',
            'key'          => 'youtube',
         
          
        ],
        [
            'id'             => 4,
            'value'           => '',
            'key'          => 'phoneOne',
      
        ],
        [
            'id'             => 5,
            'value'           => '',
            'key'          => 'phoneTwo',
   
          
        ],

        [
            'id'             => 6,
            'value'           => '',
            'key'          => 'Mail',
      
          
        ]
    ];

    settings::insert($users);
    }
}
