<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('headingen');
            $table->string('headingar');            
            $table->text('descriptionen');
            $table->text('descriptionar');
            $table->string('imageen');
            $table->string('imagear');
            $table->string('type');
            $table->string('number');
            $table->text('map')->nullable();
            $table->text('meta_description');
            $table->text('meta_keywords');
            
    
             
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
