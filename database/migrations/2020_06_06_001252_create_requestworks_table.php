<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestworksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requestworks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->String('name');
            $table->String('Email');
            $table->string('number');
            $table->string('buyarea');
            $table->string('area');   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requestworks');
    }
}
