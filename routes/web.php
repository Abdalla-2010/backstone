<?php

Route::redirect('/', '/login');

Route::redirect('/home', '/admin');

Auth::routes(['register' => false]);
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');

    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');

    Route::resource('permissions', 'PermissionsController');

    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');

    Route::resource('roles', 'RolesController');

    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    Route::delete('products/destroy', 'ProductsController@massDestroy')->name('products.massDestroy');
    Route::resource('products', 'ProductsController');
    //start about table
Route::get('aboutback/{id}','backendadmin@show');
Route::post('aboutsto/{id}','backendadmin@about');
Route::get('all','backendadmin@all');
  //end about table
 //start project 

Route::get('Details','backendadmin@Details');
Route::get('showDetails/{id}','backendadmin@shownew');
Route::post('store','backendadmin@sa');

Route::get('create','backendadmin@create');
//end project 
//startshow request front 
Route::get('showRequest','backendadmin@showRequest');

//end show request front 
//start setting 
Route::get('setting','backendadmin@setting');
Route::get('settingDetails/{id}','backendadmin@settingDetails');
Route::delete('delete/{id}','backendadmin@destroy');
Route::get('editproject/{id}','backendadmin@editproject');
Route::post('settingstro/{id}','backendadmin@settingstro');
Route::post('edite/{id}','backendadmin@Updateproject');
//end setting
//start service 
Route::get('service','backendadmin@service');
Route::get('contactus','backendadmin@contact');

Route::get('service_edit/{id}','backendadmin@serviceedit');
Route::post('service_save/{id}','backendadmin@servicesave');
Route::get('video/','backendadmin@video');
Route::post('uploadvideo/{id}','backendadmin@uploadvideo');
//end service
}); 
Route::get('/','Front@index')->name('/');
Route::get('aboutus','Front@about');
Route::get('Contact','Front@Contact');
Route::get('current_project','Front@projects');
Route::get('Previous_project','Front@Previous_project');
Route::get('services','Front@services');

Route::get('Details/{id}','Front@Details');

Route::post('sendrequesinformation','Front@Requestin');
Route::post('sendmall','Front@sendmall');

Route::get('locale/{locale}' ,function($locale){
  session::put('locale',$locale);
  session::save();

  return redirect(url(URL::previous()));
});
