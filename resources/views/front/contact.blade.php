@extends('front.layout.app')
@section('content')

<div class="custom">
<div id="slider" class="slido">
@include('front.navbar')

</div>
</div>  </div>
<div id="wrapper">

<header id="header" itemscope itemtype="http://schema.org/WPHeader">
<div class="inside">
<a href="https://www.cssdesignawards.com/sites/kwer/35180/" target="_blank">
<div id="cssda-badge">
<img src="files/awards/cssda-special-kudos-white.png" width="90" height="90" alt="CSS Award" />
</div>
</a>            </div>
</header>
<div class="container ce" id="container">
<main id="main" itemscope itemtype="http://schema.org/WebPageElement" itemprop="mainContentOfPage">
<div class="inside">
<div class="mod_article raw conti first last block" id="article-6">
<h1 class="ce_headline first" style="letter-spacing:1px;color:#FFF; font-size:18px; 
    @if(app()->getLocale() =='ar')

font-family:'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;

@endif ">
@if(app()->getLocale() =='ar')
اتصل بنا
@elseif(app()->getLocale() =='en')
Contact Us
@endif
</h1>
	<div style="direction: rtl;">
<div class="container ce">
	<div class="row">
<div class="col-xs-12 col-md-6">
	<form method="post" action="{{url('sendmall')}}">
	@csrf

		<div class="col-md-6">
			    <div class="form-group" style="margin-top: 15px;">
 
<input type="text" name="name" class="form-control ar" placeholder="@if(app()->getLocale() =='ar')
الاسم
@elseif(app()->getLocale() =='en')
name
@endif " id="name">
</div>
		</div>

		<div class="col-md-6">
		    <div class="form-group" style="margin-top: 15px;">
 
<input type="text" name="number" class="form-control ar" placeholder="@if(app()->getLocale() =='ar')
رقم الهاتف
@elseif(app()->getLocale() =='en')
Number Phone
@endif " id="phone">
		
		</div>
	</div>
		<div class="col-md-12" style="margin-top: 25px;">
				    <div class="form-group">
 
<input type="Email" name="Email"  class="form-control ar" placeholder="@if(app()->getLocale() =='ar')
البريد الالكتروني
@elseif(app()->getLocale() =='en')
Email
@endif " id="email">
</div>	
		</div>
		<div class="col-md-12" style="margin-top: 25px;">
				    <div class="form-group">

<textarea name="subject" class="form-control ar"  placeholder=" @if(app()->getLocale() =='ar')
 اترك رسالتك
@elseif(app()->getLocale() =='en')
Enter your massage
@endif  "  style="height: 70px;"></textarea>
</div>	
		</div>
		<div class="col-md-12">
		<div class="form-group">
<input type="submit" value="@if(app()->getLocale() =='ar')
اتصل بنا
@elseif(app()->getLocale() =='en')
Contact Us
@endif
"  class="btn  cont " style=" 
background:#FFF;color:#000;">
	</div>	</div>
	</form>
</div>
<div class="col-xs-12 col-md-6">
	<img src="{{asset('assets/images/logo/Logo.png')}}" class="im" >
</div>

	</div>
	<div  class="col-xs-12 " style="margin-top: 35px;" >
<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d13832.113074782987!2d31.0679065!3d29.921088349999998!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sar!2seg!4v1591472966813!5m2!1sar!2seg"  allowfullscreen="" aria-hidden="false" tabindex="0" class="maping"></iframe>		
	</div>
</div>

@endsection
