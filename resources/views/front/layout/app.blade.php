<!DOCTYPE html>
<html >
<head>
<link rel="shortcut icon" href="image/Logo" type="image/png">
<link href='https://fonts.googleapis.com/css?family=Noto+Sans' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Amiri' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=poppins' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=BigVesta' rel='stylesheet' type='text/css'>



<meta charset="utf-8">
<title>Backstone</title>
<meta name="description" content="Backstone">
<meta name="generator" content="Backstone">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="{{asset('assets/css/all.css')}}">
<link rel="stylesheet" href="a{{asset('assets/css/animate.css')}}">
<link rel="stylesheet" href="{{asset('assets/css/3abd10cdfe1c.css')}}">
<script src="{{asset('assets/js/9aa1d49c2e24.js')}}"></script>
<link type="text/css" media="screen" rel="stylesheet" href="{{asset('cssda.css')}}" />
<script src="{{asset('js/viewportSize-min.js')}}"></script>
<script src="{{asset('js/viewport-scale.js')}}"></script>
<link rel="stylesheet" href="{{asset('css/all.css')}}">
<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">

<link type="text/css" rel="stylesheet" href="{{asset('Amiri-Bold.ttf')}}" />
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700&amp;display=swap" rel="stylesheet">
<link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"
  />
  <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800&amp;display=swap" rel="stylesheet">
<style>@supports (-webkit-overflow-scrolling:touch){.video{position:absolute!important;left:50%!important;margin-left:0%!important;top:0px!important;z-index:-10!important;margin-top:0!important;margin-left:-50%!important;width:100%}.videokopf{display:none}.mobilekopf{display:block}}</style>
<style type="text/css">
  .navbar-toggle {
color: #FFF;    margin-top: 0px; 
    padding: 0px 10px;

}.df{
  direction: rtl;

}
.mob2{
  width:50%;
}
body{
  overflow-x:hidden;
  font-family: 'poppins'  ;
  ;
}.overlay {
  /* Height & width depends on how you want to reveal the overlay (see JS below) */   
  height: 100%;
  width: 0;
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  
  background-color: rgb(0,0,0); /* Black fallback color */
  background-color: rgba(0,0,0, 1); /* Black w/opacity */
  overflow-x: hidden;
   /* Disable horizontal scroll */
  transition: 0.5s; /* 0.5 second transition effect to slide in or slide down the overlay (height or width, depending on reveal) */
}a{
  text-decoration:none;
}

/* Position the content inside the overlay */
.overlay-content {
  position: relative;
  top: 25%; /* 25% from the top */
  width: 100%; /* 100% width */
  text-align: center; /* Centered text/links */
  margin-top: 30px; /* 30px top margin to avoid conflict with the close button on smaller screens */
}

/* The navigation links inside the overlay */
.overlay a {
  padding: 8px;
  text-decoration: none;
  font-size: 36px;
  color: #818181;
  display: block; /* Display block instead of inline */
  transition: 0.3s; /* Transition effects on hover (color) */
}

/* When you mouse over the navigation links, change their color */
.overlay a:hover, .overlay a:focus {
  color: #f1f1f1;
}

/* Position the close button (top right corner) */
.overlay .closebtn {
  position: absolute;
  top: 20px;
  right: 45px;
  font-size: 60px;
}

/* When the height of the screen is less than 450 pixels, change the font-size of the links and position the close button again, so they don't overlap */
@media screen and (max-height: 450px) {
  .overlay a {font-size: 20px}
  .overlay .closebtn {
    font-size: 40px;
    top: 15px;
    right: 35px;
  }
}
.nav-mido ul li {
  display:inline;
  @if(app()->getLocale() =='ar')

  padding-right:10px;
  padding-left:12px;
  @elseif(app()->getLocale() =='en')
padding-right:1px;
  padding-left:1px;;
@endif


}  .overlay-content a{
  font-family: 'poppins'  ;
  ;
 font-style: inherit;
    font-weight: 900;
    font-variant-caps: normal;
    flex-shrink: unset;
    
}
 a{
  @if(app()->getLocale() =='ar')

  font-family: "Amiri";
@elseif(app()->getLocale() =='en')
font-family: 'poppins'  ;

@endif
  
 font-style: inherit;
    font-weight: 900;
    font-variant-caps: normal;
    
} .menu{
 
  font-size: 18px;
  @if(app()->getLocale() =='ar')

  font-family: "Amiri";
@elseif(app()->getLocale() =='en')
font-family: 'poppins'  ;

@endif
  
 font-style: inherit;
    font-weight: 900;
    font-variant-caps: normal;
}a:hover{
  color:#FFF;
  text-decoration: none;
}
/* Button used to open the contact form - fixed at the bottom of the page */
.open-button {
  color: white;
  border-radius: 50%;
    width: 70px;
    height: 66px;
/*  cursor: pointer;
*/  opacity: 0.8;
  position: fixed;
  bottom: 84px;
    z-index: 99999;
    right: 21px;
}
.col{
  color:#FFF:
}
.open-button1 {
  color: white;
  border-radius: 50%;
    width: 90px;
    height: 86px;
/*  cursor: pointer;
*/  opacity: 0.8;
  position: fixed;left:23px;
  @if(app()->getLocale() =='ar')

  bottom: 25px;
@elseif(app()->getLocale() =='en')
bottom: 45px;

@endif
    z-index: 99999;
    right: 45px;

}.open-button2 {
  color: white;
  border-radius: 50%;
    width: 90px;
    height: 86px;
/*  cursor: pointer;
*/  opacity: 0.8;
  position: fixed;
  bottom: 23px;
    z-index: 99999;
left:23px;
}
/* The popup form - hidden by default */
.form-popup{
  display: none;
  color: #000;
  position: fixed;
    border-radius: 5px;
      font-family: "Times New Roman", Times, serif;
font-size:bold;
   border:1px solid #DDD;
  bottom: 0;
  left:23px;
  border: 3px solid #f1f1f1;
  z-index: 99999;
  max-width:300px; 
  width: 300px;

}

@if(app()->getLocale() =='ar')

.heade{
  background-color: #FFF;color: #000;
  direction: rtl;
}
.ar{
  direction: rtl;
}

@elseif(app()->getLocale() =='en')
.heade{
  background-color: #FFF;color: #000;
}
.ar{
  direction: ltr;
}
@endif  
@if(app()->getLocale() =='ar')

.form-popup input{
  direction: rtl;
      padding-right: 0px;
      margin-top:15px;
      margin-bottom: 15px;
} 

@elseif(app()->getLocale() =='en')
.form-popup input{
      padding-right: 36px;
      margin-top:15px;
      margin-bottom: 15px;
}
@endif 

/* Add styles to the form container */
.form-container {
  max-width: 336px;
  padding: 10px;

  background-color: white;
}

/* Full-width input fields */

/* Set a style for the submit/login button */
.form-container .btn {
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  width: 100%;
  margin-bottom:10px;
  opacity: 0.8;
}

/* Add a red background color to the cancel button */
.form-container .cancel {
  background-color: red;
}
#new{
  display: none;
}
/* Add some hover effects to buttons */
.form-container .btn:hover, .open-button:hover {
  opacity: 1;
}.ce{
  style=" padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;"
}.navbar-default .navbar-nav>li>a {
    color: #FFF;
    @if(app()->getLocale() =='ar')
    font-size: 24px;

font-family: "Amiri";
@elseif(app()->getLocale() =='en')
font-family: 'poppins'  ;


font-size: 21px;

@endif    font-style: inherit;
    font-weight: 900;
    font-variant-caps: normal;}
    .navbar-default .navbar-nav>li>a:hover {
    color: #000;
  }
 .newsa{
   letter-spacing:1px;
   color:#FFF;
  @if(app()->getLocale() =='ar')

font-family: "Amiri";
@elseif(app()->getLocale() =='en')
font-family: 'poppins'  ;


@endif  
 } .pagination > li:last-child > a, .pagination > li:last-child > span , .pagination > li:first-child > a, .pagination > li:first-child > span {
  background-color: #000000;
color:#FFF;    border-color: #000000;

 }
.pagination > li a{
  z-index: 3;
    color: #FFf;
    cursor: default;
    background-color: #000000;
    border-color: #000000;  
}.pagination > li a:hover{
  
    background-color: #FFF;
    border-color: #000000;  
}
.pagination > .active > a, .pagination > .active > span, .pagination > .active > a:hover, .pagination > .active > span:hover, .pagination > .active > a:focus, .pagination > .active > span:focus {
    z-index: 3;
    color: #000;
    cursor: default;
    background-color: #fff;
    border-color: #000000;
}
  .nav > li > a {
    position: relative;
    display: block;
    padding: 0px 10px;
    @if(app()->getLocale() =='ar')
    font-size: 24px;

font-family: "Amiri";
@elseif(app()->getLocale() =='en')
font-family: 'poppins'  ;

font-size: 21px;

@endif
} @if(app()->getLocale() =='ar')
  .cont{
  float:right;
  }
@elseif(app()->getLocale() =='en')
.cont{
float: left;
   
  }
@endif  

    .slido button{
      right: 0px; 
    top: -20px;
    position:none;
    margin:25px;
    border:  none;
    background:  none  ;
     z-index: none;
     padding: none;
    }
 
 

   @media (max-width:767px){
.mob{

  width: 100%;
}
.mob2{
  width:100%;
}

 .cont{
  width:100%;
  @if(app()->getLocale() =='ar')
margin-right:0px;
@elseif(app()->getLocale() =='en')
    margin-left: 0;
@endif  
margin-bottom:10px;

}.maping{
  width:100%;
  height:400px;
}
    }
  
    
  
    @media (min-width:767px and (max-width: 921px) ){
      .mob{
  width: 50%;
}
.mob2{
  width:100%;
}

      .cont{
}
  width:100%;
  @if(app()->getLocale() =='ar')
  margin-right:0px;
@elseif(app()->getLocale() =='en')

    margin-left: 0px;
@endif  
margin-bottom:10px;
}
    
.maping{
  width:100%;
  height:400px;
  
}.im{
  width:100%;
  height:200px;
} .im{
  width:100%;
  height:200px;
}
}
  
.maping{
  width:100%;
  height:400px;

}
.im{
  width:100%;
  height:300px;

}
.last {
  color:#FFF;
}.fo-float{color:#000}
strong
.fo-float strong{color:#000}
    .navbar-default .navbar-nav>li>a:hover {
       color:#FFF;
        }
        .overlay {
  /* Height & width depends on how you want to reveal the overlay (see JS below) */   
  height: 100%;
  width: 0;
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  left: 0;
  top: 0;
  background-color:#000; /* Black w/opacity */
  overflow-x: hidden; /* Disable horizontal scroll */
  transition: 0.5s; /* 0.5 second transition effect to slide in or slide down the overlay (height or width, depending on reveal) */
}

/* Position the content inside the overlay */
.overlay-content {
  position: relative;
  top: 25%; /* 25% from the top */
  width: 100%; /* 100% width */
  text-align: center; /* Centered text/links */
  margin-top: 30px; /* 30px top margin to avoid conflict with the close button on smaller screens */
}

/* The navigation links inside the overlay */
.overlay a {
  padding: 8px;
  text-decoration: none;
  font-size: 36px;
  color: #FFF;
  display: block; /* Display block instead of inline */
  transition: 0.3s; /* Transition effects on hover (color) */
}

/* When you mouse over the navigation links, change their color */
.overlay a:hover, .overlay a:focus {
  color: #f1f1f1;
}

/* Position the close button (top right corner) */
.overlay .closebtn {
  position: absolute;
  top: 20px;
  right: 45px;
  font-size: 60px;
}

/* When the height of the screen is less than 450 pixels, change the font-size of the links and position the close button again, so they don't overlap */
@media screen and (max-height: 450px) {
  .overlay a {font-size: 20px}
  .overlay .closebtn {
    font-size: 40px;
    top: 15px;
    right: 35px;
  }
}
@media (max-width:767px){
.fontmob{
  font-size:10px;

  color:#FFF;
}
}

}
@media (min-width: 768px) and (max-width: 991px){
  .fontmob{
  font-size:15px;

  color:#FFF;
}  .clean1{
        clear: both;
        }
}
@media (min-width: 992px) and (max-width: 1449px){
  .fontmob{
  font-size:20px;

  color:#FFF;
}

    </style>
    <style>
-------------------------------------------------------- */
footer {
  position: relative;
  z-index: 3;
  background: #000;
  padding: 0 50px 30px;    font-family: "Montserrat", sans-serif;

}

@media only screen and (max-width: 767px) {
  footer {
    margin-left: 0;
    padding: 0 30px 30px 30px;    font-family: "Montserrat", sans-serif;

  }
}

@media only screen and (max-width: 575px) {
  footer {
    padding-left: 15px;    font-family: "Montserrat", sans-serif;

  }
}

footer a,
footer p {
  color: rgba(194, 194, 194, 0.6);
  margin-bottom: 0;
  font-size: 11px;
  font-weight: 700;
  text-transform: uppercase;
  letter-spacing: 4px;
  line-height: 1.6;
  font-variant-numeric: proportional-nums;
  font-family: "Montserrat", sans-serif;
}

.v-light footer a, .v-light
footer p {
  color: #0009;    font-family: "Montserrat", sans-serif;

}

@media only screen and (max-width: 767px) {
  footer a,
  footer p {
    letter-spacing: 0;
  }
}

footer .contact-footer {
  display: -webkit-box;    font-family: "Montserrat", sans-serif;

  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  -webkit-box-pack: start;
  -ms-flex-pack: start;
  justify-content: flex-start;
  margin-bottom: 10px;
}

footer .contact-footer .phone {
  margin-right: 30px;
}

footer .copyright-social {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: horizontal;
  -webkit-box-direction: normal;
  -ms-flex-direction: row;
  flex-direction: row;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
  -webkit-box-pack: justify;
  -ms-flex-pack: justify;
  justify-content: space-between;
}

@media only screen and (max-width: 767px) {
  footer .copyright-social {
    -webkit-box-align: start;
    -ms-flex-align: start;
    align-items: start;
    -webkit-box-orient: inherit;
    -webkit-box-direction: inherit;
    -ms-flex-direction: inherit;
    flex-direction: inherit;
  }

}

@media only screen and (max-width: 575px) {
  footer .copyright-social {
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
  }
}

@media only screen and (max-width: 767px) {
  footer .copyright-social p {
    -webkit-box-ordinal-group: 3;
    -ms-flex-order: 2;
    order: 2;
  }
}

footer .copyright-social ul {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-align: center;
  -ms-flex-align: center;
  align-items: center;
}

@media only screen and (max-width: 767px) {
  footer .copyright-social ul {
    margin-top: 0;
    margin-bottom: 10px;
  }
}

footer .copyright-social ul li {
  display: list-item;
  margin-right: 20px;
  list-style: none;
}

footer .copyright-social ul li:last-child {
  margin-right: 0;
}

.footer {
  background-color: #fff;
}

.v-light .footer {
  background-color: #e6e6e6;
}

.footer .footer-links {
  padding-top: 80px;
  padding-bottom: 80px;
}

@media only screen and (max-width: 767px) {
  .footer .footer-links .dsn-col-footer:nth-of-type(n+2) {
  }
}

.footer .footer-links .footer-title {
  position: relative;
  letter-spacing:1px; color: #000;
  text-transform: uppercase;
  font-weight: 700;
  font-size: 15px;
  padding-bottom: 10px;
  margin-bottom: 25px;
  letter-spacing: 2px;
}

.v-light .footer .footer-links .footer-title {
  color: #000;
}



.v-light .footer .footer-links .footer-title::after {
  background-color: #000;
}

.footer .footer-links .footer-block.col-menu ul li {
  font-size: 14px;
  font-weight: 600;
  margin-bottom: 10px;
  overflow: hidden;
}

.footer .footer-links .footer-block.col-menu ul li:last-of-type {
  margin-bottom: 0;
}

.footer .footer-links .footer-block.col-menu ul li a {
  font-weight: 600;
  letter-spacing: 0px;
}

.footer .footer-links .footer-block.col-contact p {
  font-size: 12px;
  font-weight: 600;
  margin-bottom: 10px;
}

.footer .footer-links .footer-block.col-contact p strong {
  color: #000;
}

.v-light .footer .footer-links .footer-block.col-contact p strong {
  color: #000;
}

.footer .footer-links .footer-block.col-contact p span {
  margin-right: 5px;
  margin-left: 5px;
}

.footer .footer-links .footer-block .footer-logo a{
font-size: 20px;
color: #000;
}

.footer .footer-links .footer-block .footer-social {
  margin-top: 20px;
}

.footer .footer-links .footer-block .footer-social ul li {
  display: inline-block;
  list-style: none;
  margin-right: 5px;
}

.footer .footer-links .footer-block .footer-social ul li:last-child {
  margin-right: 0;
}

.footer .footer-links .footer-block .footer-social ul li a {
  position: relative;
  color: #000;
  border-radius: 50%;
  letter-spacing: 0;
  border: 1px solid rgba(255, 255, 255, 0.07);
  height: 35px;
  width: 35px;
  line-height: 35px;
  text-align: center;
  -webkit-transition: -webkit-transform 0.2s ease-in-out;
  transition: -webkit-transform 0.2s ease-in-out;
  -o-transition: transform 0.2s ease-in-out;
  transition: transform 0.2s ease-in-out;
  transition: transform 0.2s ease-in-out, -webkit-transform 0.2s ease-in-out;
}

.v-light .footer .footer-links .footer-block .footer-social ul li a {
  color: #000;
  border: 1px solid #bebebe;
}

.footer .footer-links .footer-block .footer-social ul li a i {
  position: relative;
  z-index: 2;
  -webkit-transition: -webkit-transform 0.2s ease-in-out;
  transition: -webkit-transform 0.2s ease-in-out;
  -o-transition: transform 0.2s ease-in-out;
  transition: transform 0.2s ease-in-out;
  transition: transform 0.2s ease-in-out, -webkit-transform 0.2s ease-in-out;
}

.footer .footer-links .footer-block .footer-social ul li a:before {
  content: "";
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  opacity: 0;
  border-radius: 50%;
  -webkit-transform: scale(0);
  -ms-transform: scale(0);
  transform: scale(0);
  background-color: #fff;
  -webkit-transform-origin: center center;
  -ms-transform-origin: center center;
  transform-origin: center center;
  -webkit-transition: opacity 0.3s cubic-bezier(0.215, 0.61, 0.355, 1), -webkit-transform 0.3s cubic-bezier(0.215, 0.61, 0.355, 1);
  transition: opacity 0.3s cubic-bezier(0.215, 0.61, 0.355, 1), -webkit-transform 0.3s cubic-bezier(0.215, 0.61, 0.355, 1);
  -o-transition: transform 0.3s cubic-bezier(0.215, 0.61, 0.355, 1), opacity 0.3s cubic-bezier(0.215, 0.61, 0.355, 1);
  transition: transform 0.3s cubic-bezier(0.215, 0.61, 0.355, 1), opacity 0.3s cubic-bezier(0.215, 0.61, 0.355, 1);
  transition: transform 0.3s cubic-bezier(0.215, 0.61, 0.355, 1), opacity 0.3s cubic-bezier(0.215, 0.61, 0.355, 1), -webkit-transform 0.3s cubic-bezier(0.215, 0.61, 0.355, 1);
}

.v-light .footer .footer-links .footer-block .footer-social ul li a:before {
  background-color: #000;
}

.footer .footer-links .footer-block .footer-social ul li a:hover i {
  color: #000;
  -webkit-transform: rotate(360deg);
  -ms-transform: rotate(360deg);
  transform: rotate(360deg);
}

.v-light .footer .footer-links .footer-block .footer-social ul li a:hover i {
  color: #000;
}

.footer .footer-links .footer-block .footer-social ul li a:hover:before {
  opacity: 1;
  -webkit-transform: scale(1);
  -ms-transform: scale(1);
  transform: scale(1);
  -webkit-transition: opacity 0.5s cubic-bezier(0.215, 0.61, 0.355, 1) 0.2s, -webkit-transform 0.5s cubic-bezier(0.23, 1, 0.32, 1) 0.2s;
  transition: opacity 0.5s cubic-bezier(0.215, 0.61, 0.355, 1) 0.2s, -webkit-transform 0.5s cubic-bezier(0.23, 1, 0.32, 1) 0.2s;
  -o-transition: transform 0.5s cubic-bezier(0.23, 1, 0.32, 1) 0.2s, opacity 0.5s cubic-bezier(0.215, 0.61, 0.355, 1) 0.2s;
  transition: transform 0.5s cubic-bezier(0.23, 1, 0.32, 1) 0.2s, opacity 0.5s cubic-bezier(0.215, 0.61, 0.355, 1) 0.2s;
  transition: transform 0.5s cubic-bezier(0.23, 1, 0.32, 1) 0.2s, opacity 0.5s cubic-bezier(0.215, 0.61, 0.355, 1) 0.2s, -webkit-transform 0.5s cubic-bezier(0.23, 1, 0.32, 1) 0.2s;
}

.footer .footer-links .footer-block .footer-social ul li a i {
  font-size: 12px;
}

.footer .copyright {
  padding-top: 30px;
  font-size: 14px;
  letter-spacing: 2.67px;
  border-top: 1px solid rgba(255, 255, 255, 0.1);
}

.v-light .footer .copyright {
  border-top: 1px solid #bebebe;
}

.footer .copyright p {
  text-transform: uppercase;
}

.footer .copyright .copright-text {
  color: #000;

  margin-top: 5px;
  font-weight: 500;
}

.footer .copyright .copright-text a {
  color: #fff;
}

.v-light .footer .copyright .copright-text a {
  color: #000;

}

.cap {
  position: absolute;
  bottom: 30px;
  left: 0;
  background-image: -webkit-gradient(linear, left top, right top, from(#0e0e0e5c), to(#1b1515));
  background-image: -webkit-linear-gradient(left, #0e0e0e5c 0%, #1b1515 100%);
  background-image: -o-linear-gradient(left, #0e0e0e5c 0%, #1b1515 100%);
  background-image: linear-gradient(to right, #0e0e0e5c 0%, #1b1515 100%);
  padding: 4px 15px;
  color: #fff;
  z-index: 10;
}

.cap span {
  font-size: 12px;
  text-transform: uppercase;
  letter-spacing: 2px;
}

.caption {
  left: -20px;
  bottom: 0px;
  -webkit-transform: rotate(-90deg);
  -ms-transform: rotate(-90deg);
  transform: rotate(-90deg);
  -webkit-transform-origin: left center;
  -ms-transform-origin: left center;
  transform-origin: left center;
  color: #000;
  letter-spacing: 2px;
  font-size: 12px;
  font-weight: 600;
  position: absolute;
}

.v-light .caption {
  color: #000;
}
        .clean{
        clear: both;
        }
/*# sourceMappingURL=style.css.map */
.fa-facebook-f:before, .fa-facebook:before {
    content: "\f09a";
    font-size: 30px;
}
.fa-behance:before {
    content: "\f1b4";
    font-size: 30px;

}.fa-instagram:before {
    content: "\f16d";
        font-size: 30px;

}
.fa-twitter:before {
    content: "\f099";
            font-size: 30px;

}
</style>
<style type="text/css">
  .topbar::after {
  content: "";
  position: absolute;
  top: 0;
      right: 65%;
    background: grey;
    width: 122px;
  height: 68px;
    -webkit-transform: skew(30deg);
  -moz-transform: skew(30deg);
  -o-transform: skew(30deg);
  z-index: 999999;
}.co{
   background:  grey;
   color: #000;
   text-align: center;
  height: 68px;
  ;position: absolute;
    z-index: 99999;
}
.co1{
   background: #000;
   color: #fff;

   text-align: center;
     margin-right: -29px;
border:1px solid #2d2c2c;
  height: 68px;}
.topbar    ul li {

  display: inline-block;
  padding:10px;
}
.topbar    ul li a{
  color: #fff;

}a{
  letter-spacing: 1px;
}
   @media (min-width:992px){
.footer-social1{
  margin:0;padding:0px;float:left
}
.footer .footer-logo a{
color:#000;font-size:20px;float:left;
} 
.fo-left{
  left: -15px; position: absolute;
}.fo-left li a {
  color:#000;
  letter-spacing:0px;
}
.fo-float{float: left;}
.footer .footer-links .footer-title::after {
  content: "";
  position: absolute;
  left: 0;
  bottom: 0;
  width: 15px;
  height: 2px;
  background-color: #fff;
}.footer-title{
            float: left;
        }
            hr{
              display: none;
            }
        

.footer .footer-links .footer-block .footer-logo a     {
      margin-top: -20px;
  }    }
                              @media (max-width:991px){
.clean1{
  clear: both;
}
           hr{
    width: 200px;
    height: 1px;
    background: #FFF;

            }   
            .topbar{
              display: none;
            }
            .navbar-toggle {
    display: block;
}
                   .logo {
    width: 15%;
    left: 0px;
    top: 5px;
}     .jumbotron p {
    font-size: 15px;                            }
}.jumbotron{
  background:#FFF
}
</style>
</head>

<body style="background: #fff;">

            @yield("content")
   <footer class="footer">
                <div class="container">
                    <div class="footer-links p-relative">
                        <div class="row">
                            <div class="col-md-3 col-xs-12 dsn-col-footer">
                                <div class="footer-block">
                                    <div class="footer-logo">
                                        <a href=""> Backstone</a>
                                                                              <div class="clean1"></div>

                                    </div>
                                    <div class="footer-social">

                                        <ul class="footer-social1">
                                            <li><a href="{{$Facebook->value}}"><i class="fab fa-facebook fa-4x"></i></a></li>
                                            <li><a href="{{$instagram->value}}"><i class="fab fa-instagram fa-4x"></i></a></li>
                                            <li><a href="{{$youtube->value}}"><i class="fab fa-twitter fa-4x"></i></a></li>

                                        </ul>

                                    </div>
                                </div>
                            </div>
                                                                                                              <div class="clean1"></div>
                                                                                                              <hr>

                            <div class="col-md-3   col-xs-12 dsn-col-footer">
                                <div class="footer-block col-menu">
                                    <h4 class="footer-title"  style="    letter-spacing: 1px;
"> 
@if(app()->getLocale() =='ar')

روابط سريعه

@elseif(app()->getLocale() =='en')
Quick links
@endif</h4>
                                    <div class="clean"></div>
                                  <nav>
                                        <ul class="fo-left">
                                            <li><a  href="{{url('aboutus')}}"> 
 @if(app()->getLocale() =='ar')

معلومات عنا

@elseif(app()->getLocale() =='en')
About us
@endif</a>
                                            </li>
                                            <li><a href="{{url('Contact')}}">
@if(app()->getLocale() =='ar')
اتصل بنا
  @elseif(app()->getLocale() =='en')
Contact us
@endif
</a></li>
                                            <li><a href="{{url('current_project')}}">@if(app()->getLocale() =='ar')
المشروعات  الحالية
@elseif(app()->getLocale() =='en')
Available project
@endif</a></li>
                                            <li><a href="{{url('Previous_project')}}">@if(app()->getLocale() =='ar')
المشروعات  السابقة
@elseif(app()->getLocale() =='en')
Previous project
@endif</a>
                                            </li>

                                               <li><a  href="{{url('services')}}" >@if(app()->getLocale() =='ar')
خدماتنا
@elseif(app()->getLocale() =='en')
Services
@endif</a>
                                            </li>

                                        </ul>
                                    </nav>
                                </div>
                            </div>

                                                                                                              <div class="clean1"></div>
                                                                                                              <hr>

                            <div class="col-md-3 dsn-col-footer">
                                <div class="footer-block col-contact"  class="fo-left">
                                    <h4 class="footer-title" style="    letter-spacing: 1px;
">@if(app()->getLocale() =='ar')
اتصل بنا
  @elseif(app()->getLocale() =='en')
Contact us
@endif</h4>
                                      <div class="clean1"></div>
                                                                                                              <div class="clean"></div>

                                    <p class="fo-float"><strong>T</strong> <span>:</span> {{$phoneOne->value}}</p>
                                                                          <div class="clean"></div>

                                    <p class="fo-float"><strong>T</strong> <span>:</span>{{$phoneTwo->value}}  </p>
                                                                                                              <div class="clean"></div>

                                    <p style="width:86%;" class=" fo-float"><strong>E</strong> <span>:</span>{{$Mail->value}}

                                    </p>
                                </div>
                            </div>


                                                                                                              <div class="clean1"></div>
                                                                                                              <hr>
                            <div class="col-md-3  col-xs-12 dsn-col-footer">
                                <div class="col-address">
                                    <h4 class="footer-title"  style="    letter-spacing: 1px;
">
@if(app()->getLocale() =='ar')
عنوان
  @elseif(app()->getLocale() =='en')
Address
@endif
</h4>
                                      <div class="clean"></div>

                                    <p class="fo-float"style="font-size:15px;">@if(app()->getLocale() =='ar')

{{$Adress->contentar}}
  @elseif(app()->getLocale() =='en')
{{$Adress->contenten}}

Address
@endif
</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="copyright">
                        <div class="text-center">
                            <p>© copyright©SoftwareRoute 2020</p>
                        
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
<div><h4 style="color:#FFF; "> </h4></div>

<script>window.addEvent('domready',function(){new Fx.Accordion($$('.toggler'),$$('.accordion'),{opacity:!1,alwaysHide:!0,display:-1,onActive:function(tog,el){tog.addClass('active');tog.setProperty('aria-expanded','true');el.setProperty('aria-hidden','false');return!1},onBackground:function(tog,el){tog.removeClass('active');tog.setProperty('aria-expanded','false');el.setProperty('aria-hidden','true');return!1}});$$('.toggler').each(function(el){el.setProperty('role','tab');el.setProperty('tabindex',0);el.addEvents({'keypress':function(event){if(event.code==13||event.code==32){this.fireEvent('click')}},'focus':function(){this.addClass('hover')},'blur':function(){this.removeClass('hover')},'mouseenter':function(){this.addClass('hover')},'mouseleave':function(){this.removeClass('hover')}})});$$('.ce_accordion').each(function(el){el.setProperty('role','tablist')});$$('.accordion').each(function(el){el.setProperty('role','tabpanel')})})</script>
<script>setTimeout(function(){j(".overlay_mainer").fadeOut("slow")},2000);var j=jQuery.noConflict();j(document).ready(function(){j("#wrapper").css({"opacity":"1"});j(".navbar a").click(function(){j(".navbar").css({"transform":"translate(0px,-220px)"});j("#logo").css({"transform":"translate(0px,-220px)"});j("#naviga").css({"transform":"translate(0px,-220px)"});j("#wrapper").delay(200).css({"transform":"translate(0px,-50px)"});j(".navbar").css({"-moz-transform":"translate(0px,-220px)"});j("#logo").css({"-moz-transform":"translate(0px,-220px)"});j("#naviga").css({"-moz-transform":"translate(0px,-220px)"});j("#wrapper").delay(200).css({"-moz-transform":"translate(0px,-50px)"});j(".navbar").css({"-webkit-transform":"translate(0px,-220px)"});j("#logo").css({"-webkit-transform":"translate(0px,-220px)"});j("#naviga").css({"-webkit-transform":"translate(0px,-220px)"});j("#wrapper").delay(200).css({"-webkit-transform":"translate(0px,-50px)"});j("#wrapper").delay(200).css({"opacity":"0"});var href=j(this).attr('href');setTimeout(function(){window.location=href},300);return!1});j("#logo img").click(function(){j(".navbar").css({"transform":"translate(0px,-220px)"});j("#logo").css({"transform":"translate(0px,-220px)"});j("#naviga").css({"transform":"translate(0px,-220px)"});j("#wrapper").delay(200).css({"transform":"translate(0px,-50px)"});j(".navbar").css({"-moz-transform":"translate(0px,-220px)"});j("#logo").css({"-moz-transform":"translate(0px,-220px)"});j("#naviga").css({"-moz-transform":"translate(0px,-220px)"});j("#wrapper").delay(200).css({"-moz-transform":"translate(0px,-50px)"});j(".navbar").css({"-webkit-transform":"translate(0px,-220px)"});j("#logo").css({"-webkit-transform":"translate(0px,-220px)"});j("#naviga").css({"-webkit-transform":"translate(0px,-220px)"});j("#wrapper").delay(200).css({"-webkit-transform":"translate(0px,-50px)"});j("#wrapper").delay(200).css({"opacity":"0"});var href=j(this).attr('href');setTimeout(function(){window.location=href},300);return!1});j(".karriere a").click(function(){j(".navbar").css({"transform":"translate(0px,-220px)"});j("#logo").css({"transform":"translate(0px,-220px)"});j("#naviga").css({"transform":"translate(0px,-220px)"});j("#wrapper").delay(200).css({"transform":"translate(0px,-50px)"});j(".navbar").css({"-moz-transform":"translate(0px,-220px)"});j("#logo").css({"-moz-transform":"translate(0px,-220px)"});j("#naviga").css({"-moz-transform":"translate(0px,-220px)"});j("#wrapper").delay(200).css({"-moz-transform":"translate(0px,-50px)"});j(".navbar").css({"-webkit-transform":"translate(0px,-220px)"});j("#logo").css({"-webkit-transform":"translate(0px,-220px)"});j("#naviga").css({"-webkit-transform":"translate(0px,-220px)"});j("#wrapper").delay(200).css({"-webkit-transform":"translate(0px,-50px)"});j("#wrapper").delay(200).css({"opacity":"0"});var href=j(this).attr('href');setTimeout(function(){window.location=href},300);return!1});j(".projektstarten a").click(function(){j(".navbar").css({"transform":"translate(0px,-220px)"});j("#logo").css({"transform":"translate(0px,-220px)"});j("#naviga").css({"transform":"translate(0px,-220px)"});j("#wrapper").delay(200).css({"transform":"translate(0px,-50px)"});j(".navbar").css({"-moz-transform":"translate(0px,-220px)"});j("#logo").css({"-moz-transform":"translate(0px,-220px)"});j("#naviga").css({"-moz-transform":"translate(0px,-220px)"});j("#wrapper").delay(200).css({"-moz-transform":"translate(0px,-50px)"});j(".navbar").css({"-webkit-transform":"translate(0px,-220px)"});j("#logo").css({"-webkit-transform":"translate(0px,-220px)"});j("#naviga").css({"-webkit-transform":"translate(0px,-220px)"});j("#wrapper").delay(200).css({"-webkit-transform":"translate(0px,-50px)"});j("#wrapper").delay(200).css({"opacity":"0"});var href=j(this).attr('href');setTimeout(function(){window.location=href},300);return!1});j(".copyright a").click(function(){j(".navbar").css({"transform":"translate(0px,-220px)"});j("#logo").css({"transform":"translate(0px,-220px)"});j("#naviga").css({"transform":"translate(0px,-220px)"});j("#wrapper").delay(200).css({"transform":"translate(0px,-50px)"});j(".navbar").css({"-moz-transform":"translate(0px,-220px)"});j("#logo").css({"-moz-transform":"translate(0px,-220px)"});j("#naviga").css({"-moz-transform":"translate(0px,-220px)"});j("#wrapper").delay(200).css({"-moz-transform":"translate(0px,-50px)"});j(".navbar").css({"-webkit-transform":"translate(0px,-220px)"});j("#logo").css({"-webkit-transform":"translate(0px,-220px)"});j("#naviga").css({"-webkit-transform":"translate(0px,-220px)"});j("#wrapper").delay(200).css({"-webkit-transform":"translate(0px,-50px)"});j("#wrapper").delay(200).css({"opacity":"0"});var href=j(this).attr('href');setTimeout(function(){window.location=href},300);return!1})});j('.home-slider').viewportScale({height:'100vh'});j('.home-slider-kl').viewportScale({height:'60vh'});j('.header-verlauf').viewportScale({height:'100vh'});j(".x").click(function(){var jtarget=j('.custom22'),jtoggle=j(this);jtarget.slideToggle(500,'swing')});j(".x").click(function(){var jtarget2=j('.custom22 .level_1'),jtoggle2=j(this);jtarget2.delay(300).fadeToggle(500)});j(".x").click(function(){var jtarget3=j('.logoo'),jtoggle3=j(this);jtarget3.delay(500).fadeToggle(500)});j(".x").click(function(){j('body').toggleClass('sidePanelOpen')});j("#dsgvobut").click(function(){j('.buttonhide').toggleClass('buttonshow')})</script>
<script>var j=jQuery.noConflict();var jcontent=j('header .kwerslider'),jcont=j('header .kwerslider h1'),jcont2=j('header .kwerslider p'),jbuch=j('header .dreh'),jblur=j('header img'),jblur2=j('.one .jumbotron .mod_newslist:before'),wHeight=j(window).height();j(window).on('resize',function(){wHeight=j(window).height()});window.requestAnimFrame=(function(){return window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||function(callback){window.setTimeout(callback,1000/60)}})();function Scroller(){this.latestKnownScrollY=0;this.ticking=!1}
Scroller.prototype={init:function(){window.addEventListener('scroll',this.onScroll.bind(this),!1)},onScroll:function(){this.latestKnownScrollY=window.scrollY;this.requestTick()},requestTick:function(){if(!this.ticking){window.requestAnimFrame(this.update.bind(this))}
this.ticking=!0},update:function(){var currentScrollY=this.latestKnownScrollY;this.ticking=!1;var slowScroll=currentScrollY/4,slowScroll2=currentScrollY/2.5,slow2Scroll=100-currentScrollY*0.1,blurScroll=0.3-currentScrollY/3500,blurScroll2=1-currentScrollY/800;jcontent.css({'transform':'translateY(+'+slowScroll+'px)','-moz-transform':'translateY(+'+slowScroll+'px)','-webkit-transform':'translateY(+'+slowScroll+'px)'});jbuch.css({'transform':'rotate(-'+slowScroll2+'deg)','-moz-transform':'rotate(-'+slowScroll2+'deg)','-webkit-transform':'rotate(-'+slowScroll2+'deg)'});jcont.css({'opacity':blurScroll2});jcont2.css({'opacity':blurScroll2});jblur2.css({'transform':'translateY(+'+slowScroll+'px)','-moz-transform':'translateY(+'+slowScroll+'px)','-webkit-transform':'translateY(+'+slowScroll+'px)'})}};var scroller=new Scroller();scroller.init()</script>
<script>
  jQuery(document).ready(function(){jQuery('#header').addClass("hidden2").viewportChecker({classToAdd:'visible animated fadeInDown',offset:90})});jQuery(document).ready(function(){jQuery('#container .mod_rocksolid_slider').addClass("hidden2").viewportChecker({classToAdd:'visible animated fadeInUp',offset:90})});jQuery(document).ready(function(){jQuery('#container .layout_latest').addClass("hidden2").viewportChecker({classToAdd:'visible animated fadeInUp',offset:90})});jQuery(document).ready(function(){jQuery('#container .ce_text').addClass("hidden2").viewportChecker({classToAdd:'visible animated fadeInUp',offset:90})});jQuery(document).ready(function(){jQuery('.jumbotron .container').addClass("hidden2").viewportChecker({classToAdd:'visible animated fadeInUp',offset:90})});jQuery(document).ready(function(){jQuery('.jumbotron .ce_hyperlink').addClass("hidden2").viewportChecker({classToAdd:'visible animated fadeInUp',offset:90})});jQuery(document).ready(function(){jQuery('.jumbotron .rss_default').addClass("hidden2").viewportChecker({classToAdd:'visible animated fadeInUp',offset:90})});jQuery(document).ready(function(){jQuery('.jumbotron .teamsuche .ce_text').addClass("hidden2").viewportChecker({classToAdd:'visible animated fadeInUp',offset:90})});jQuery(document).ready(function(){jQuery('.jumbotron .rsts-crop').addClass("hidden2").viewportChecker({classToAdd:'visible animated fadeInUp',offset:90})});jQuery(document).ready(function(){jQuery('.layout_latest').addClass("hidden2").viewportChecker({classToAdd:'visible animated fadeInUp',offset:90})});jQuery(document).ready(function(){jQuery('.two .ce_text').addClass("hidden2").viewportChecker({classToAdd:'visible animated fadeInUp',offset:90})});j(".one header .ce_text a").click(function(){j('html, body').animate({scrollTop:j("#container").offset().top},1100,'easeOutQuart')});j(".six header .ce_text a").click(function(){j('html, body').animate({scrollTop:j("#container").offset().top},1100,'easeOutQuart')});j(".projektstarten a").hover(function(){j(".leftzeichen").css({color:'#CABAA0'})},function(){j(".leftzeichen").css({color:'#1F1F1F'})});j(".karriere a").hover(function(){j(".rightzeichen").css({color:'#CABAA0'})},function(){j(".rightzeichen").css({color:'#1F1F1F'})});j('.rss_default h2 a').replaceWith(function(){return j(this).text()});j('.rss_default .description a').each(function(){j(this).replaceWith(j(this).children())});j('figure.effect-marley h2 a').replaceWith(function(){return j(this).text()});j(".two .jumbotron .ce_text").hover(function(){j(this).find("hr").animate({width:'30%'});j(this).find("img").css({opacity:'0.9'})},function(){j(this).find("hr").animate({width:'20%'});j(this).find("img").css({opacity:'0.7'})});j("header .ce_text a").mouseover(function(){j("header h1 span").css({transform:'translateY(-100%)'});j(".tteste").animate({height:'100px'})}).mouseleave(function(){j("header h1 span").css({transform:'translateY(0%)'});j(".tteste").animate({height:'0px'})});j('.projektstarter .one').click(function(){var clicks=j(this).data('clicks');if(clicks){j(this).css('background-color','rgba(255,255,255,.05)');j('#ctrl_9_0').prop('checked',!1)}else{j(this).css('background-color','rgba(255,255,255,.10)');j('#ctrl_9_0').prop('checked',!0)}
j(this).data("clicks",!clicks)});j('.projektstarter .two').click(function(){var clicks=j(this).data('clicks');if(clicks){j(this).css('background-color','rgba(255,255,255,.05)');j('#ctrl_9_1').prop('checked',!1)}else{j(this).css('background-color','rgba(255,255,255,.10)');j('#ctrl_9_1').prop('checked',!0)}
j(this).data("clicks",!clicks)});j('.projektstarter .three').click(function(){var clicks=j(this).data('clicks');if(clicks){j(this).css('background-color','rgba(255,255,255,.05)');j('#ctrl_9_2').prop('checked',!1)}else{j(this).css('background-color','rgba(255,255,255,.10)');j('#ctrl_9_2').prop('checked',!0)}
j(this).data("clicks",!clicks)});j('.projektstarter .four').click(function(){var clicks=j(this).data('clicks');if(clicks){j(this).css('background-color','rgba(255,255,255,.05)');j('#ctrl_9_3').prop('checked',!1)}else{j(this).css('background-color','rgba(255,255,255,.10)');j('#ctrl_9_3').prop('checked',!0)}
j(this).data("clicks",!clicks)});j('.projektstarter .five').click(function(){var clicks=j(this).data('clicks');if(clicks){j(this).css('background-color','rgba(255,255,255,.05)');j('#ctrl_9_4').prop('checked',!1)}else{j(this).css('background-color','rgba(255,255,255,.10)');j('#ctrl_9_4').prop('checked',!0)}
j(this).data("clicks",!clicks)});j('.projektstarter .six').click(function(){var clicks=j(this).data('clicks');if(clicks){j(this).css('background-color','rgba(255,255,255,.05)');j('#ctrl_9_5').prop('checked',!1)}else{j(this).css('background-color','rgba(255,255,255,.10)');j('#ctrl_9_5').prop('checked',!0)}
j(this).data("clicks",!clicks)})</script>

<script>(function($){$.fn.countTo=function(options){options=$.extend({},$.fn.countTo.defaults,options||{});var loops=Math.ceil(options.speed/options.refreshInterval),increment=(options.to-options.from)/loops;return $(this).each(function(){var _this=this,loopCount=0,value=options.from,interval=setInterval(updateTimer,options.refreshInterval);function updateTimer(){value+=increment;loopCount++;$(_this).html(value.toFixed(options.decimals));if(typeof(options.onUpdate)=='function'){options.onUpdate.call(_this,value)}
if(loopCount>=loops){clearInterval(interval);value=options.to;if(typeof(options.onComplete)=='function'){options.onComplete.call(_this,value)}}}})};$.fn.countTo.defaults={from:0,to:100,speed:1000,refreshInterval:100,decimals:0,onUpdate:null,onComplete:null,}})(jQuery);jQuery(function($){$('.timer').countTo({from:100,to:2003,speed:1500,refreshInterval:10,onComplete:function(value){console.debug(this)}})})</script>
<script>window.requestAnimFrame=(function(callback){return window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequestAnimationFrame||window.msRequestAnimationFrame||function(callback){window.setTimeout(callback,1000/60)}})();var requestId,jolttime;var c=document.getElementById('canv');var $=c.getContext('2d');var s=18;var mv=10;var sp=1;var clm=23;var rw=10;var x=[];var y=[];var X=[];var Y=[];c.width=c.offsetWidth;c.height=c.offsetHeight;for(var i=0;i<clm*rw;i++){x[i]=((i%clm)-0.5)*s;y[i]=(Math.floor(i/clm)-0.5)*s;X[i]=x[i];Y[i]=y[i]}
var t=0;function jolt(){$.fillRect(0,0,c.width,c.height);for(var i=0;i<clm*rw;i++){if(i%clm!=clm-1&&i<clm*(rw-1)-1){$.fillStyle="hsla(0,0,0,1)";$.strokeStyle="#95D384";$.lineWidth=1;$.beginPath();$.moveTo(x[i],y[i]);$.lineTo(x[i+1],y[i+1]);$.lineTo(x[i+clm+1],y[i+clm+1]);$.lineTo(x[i+clm],y[i+clm]);$.closePath();$.stroke();$.fill()}}
for(var i=0;i<rw*clm;i++){if((x[i]<X[i]+mv)&&(x[i]>X[i]-mv)&&(y[i]<Y[i]+mv)&&(y[i]>Y[i]-mv)){x[i]=x[i]+Math.floor(Math.random()*(sp*2+1))-sp;y[i]=y[i]+Math.floor(Math.random()*(sp*2+1))-sp}else if(x[i]>=X[i]+mv){x[i]=x[i]-sp}else if(x[i]<=X[i]-mv){x[i]=x[i]+sp}else if(y[i]>=Y[i]+mv){y[i]=y[i]-sp}else if(y[i]<=Y[i]+mv){y[i]=y[i]+sp}}
if(t%c.width==0){jolttime=setTimeout('jolt()',5);t++}else{jolttime=setTimeout('jolt()',5);t++}}
function start(){if(!requestId){requestId=window.requestAnimFrame(jolt)}}
function stop(){if(requestId){clearTimeout(jolttime);window.cancelAnimationFrame(requestId);requestId=undefined}}
document.querySelector('a.link--asiri').addEventListener('mouseenter',start);document.querySelector('a.link--asiri').addEventListener('mouseleave',stop)</script>
<script>var anchor=document.querySelectorAll('.lines-button');[].forEach.call(anchor,function(anchor){var open=!1;anchor.onclick=function(event){event.preventDefault();if(!open){this.classList.add('close');open=!0}else{this.classList.remove('close');open=!1}}})</script>
<script>jQuery(document).ready(function($){var pageTitle=$("title").text();$(window).blur(function(){$("title").text("KWER - let's perform it!")});$(window).focus(function(){$("title").text(pageTitle)})})</script>
<script type="text/javascript" src="{{asset('js/jquery.js')}}"></script>

<script>
function openForm() {
  document.getElementById("myForm").style.display = "block";

       document.getElementById("myForm").style.animation="fadeIn 1s";
    document.getElementById("bu").style.display = "none";
    

}

var text = document.getElementById('text');

function closeFormas() {

      document.getElementById("bu").style.display = "block";
      var a=document.forms["va"]["name"].value;
    var b=document.forms["va"]["Email"].value;
    var c=document.forms["va"]["number"].value;
    var d=document.forms["va"]["area"].value;
    var f=document.forms["va"]["buyarea"].value;

 a = " ";
 b = '';
 c = '';
  d = '';
 f = '';
document.getElementById("new").style.display = "none";

  document.getElementById("myForm").style.display = "none";

}
function hid() {
  document.getElementById("myForm").style.display = "none";
         document.getElementById("myForm").style.animation="fadeOut 1s";
              document.getElementById("bu").style.display = "block";


}
</script>
<script>
/*       
$(document).on('click','#add',function() {
    var form=$('#A').serialize();
    var url=$('#A').attr('action');
    // alert(form)
   $.ajax({
    url:url,
    dataType:'json',
    data:form,
    type:'post',
    success:function(data){
      $('#new').show();
      document.getElementById('myImage').src='pic_bulbon.gif';
  var name = document.getElementById('name');
var email = document.getElementById('email');
var phone = document.getElementById('phone');
var buy = document.getElementById('buy');

var Area = document.getElementById('area');

 name.value = '';
 email.value = '';
 phone.value = '';
  buy.value = '';
 area.value = '';
      console.log('success');
}
   });
    // alert(url);    
return false;
}); */

validateForm();

function validateForm()
{
$(document).on('click','#add',function() {
    var form=$('#A').serialize();
    var url=$('#A').attr('action');
    // alert(form)
    var a=document.forms["va"]["name"].value;
    var b=document.forms["va"]["Email"].value;
    var c=document.forms["va"]["number"].value;
    var d=document.forms["va"]["area"].value;
    var f=document.forms["va"]["buyarea"].value;
    
    
    if (a=="" ||b=="" || c=="" || d=="" || f=="" )
    {
        alert("Please Fill All Required Field");
        return false;
    }
    else{
   $.ajax({
    url:url,
    dataType:'json',
    data:form,
    type:'post',
    success:function(data){
      $('#new').show();
      document.getElementById('myImage').src='pic_bulbon.gif';
  var name = document.getElementById('name');
var email = document.getElementById('email');
var phone = document.getElementById('phone');
var buy = document.getElementById('buy');

var Area = document.getElementById('area');

 name.value = '';
 email.value = '';
 phone.value = '';
  buy.value = '';
 area.value = '';
      console.log('success');
}
   });
}
return false;
});
}

</script>


<script src="{{asset('js/classie.js')}}"></script>
<script src="{{asset('js/viewportchecker.js')}}"></script>

<script src="{{asset('js/svganimations.js')}}"></script>
<script src="{{asset('js/bootstrap.js')}}"></script>

<script src="{{asset('assets/js/wow.min.js')}}"></script>
<script>
              new WOW().init();
              </script>

              <!-- Load Facebook SDK for JavaScript -->
      <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v7.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/ar_AR/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your Chat Plugin code -->
      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="407970489747422"
  logged_in_greeting="مرحبا كيف يمكننا مساعدتك ؟ "
  logged_out_greeting="مرحبا كيف يمكننا مساعدتك ؟ ">
      </div>



</body>

</html>