@extends('front.layout.app')
@section('content')

<script>(function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create','UA-72313087-1','auto');ga('send','pageview')</script>
<div class="custom">
<div id="slider" class="slido">
@include('front.navbar')
</div>
</div>  </div>
<div id="wrapper">
<header id="header" itemscope itemtype="http://schema.org/WPHeader">
<div class="inside">
<a href="https://www.cssdesignawards.com/sites/kwer/35180/" target="_blank">
<div id="cssda-badge">
<img src="files/awards/cssda-special-kudos-white.png" width="90" height="90" alt="CSS Award" />
</div>
</a>            </div>
</header>
<div class="container ce" id="container">
<main id="main" itemscope itemtype="http://schema.org/WebPageElement" itemprop="mainContentOfPage">
<div class="inside">
<div class="mod_article about_con first last block" id="article-2">
<h1 class="ce_headline ueber first" style="    letter-spacing: 1px;
 font-size:33px;
 @if(app()->getLocale() =='ar')
font-family: 'BigVesta';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;

@endif " >
@if(app()->getLocale() =='ar')
{{$about->headingar}}
@elseif(app()->getLocale() =='en')
{{$about->headingen}}
@endif
</h1>

<div class="ce_text hometext kwerteext abouttext block">
<h5 class="ce_headline ueber first"  style="   
@if(app()->getLocale() =='ar')
font-family: 'BigVesta';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;

@endif 
 line-height: 2;
    font-size: 19px;
    margin: 0 auto;
    padding: 10px;">
@if(app()->getLocale() =='ar')
{{$about->contentar}}

@elseif(app()->getLocale() =='en')
{{$about->contenten}}

@endif
</h5>

</div>

</div>
</div>
</main>
</div>



@endsection
