@extends('front.layout.app')
@section('content')

<script>(function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create','UA-72313087-1','auto');ga('send','pageview')</script>
<div class="custom">
<div id="slider" class="slido">
@include('front.navbar')
</div>
</div>  </div>
<div id="wrapper">
<header id="header" itemscope itemtype="http://schema.org/WPHeader">
<div class="inside">
<a href="https://www.cssdesignawards.com/sites/kwer/35180/" target="_blank">
<div id="cssda-badge">
<img src="files/awards/cssda-special-kudos-white.png" width="90" height="90" alt="CSS Award" />
</div>
</a>            </div>
</header>
<div class="container ce" id="container" > 

<main id="main" itemscope itemtype="http://schema.org/WebPageElement" itemprop="mainContentOfPage">
<div class="inside">
<h1 style="
font-size:22px;
color:#FFF;
letter-spacing:1px;
@if(app()->getLocale() =='ar')

font-family:'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;
;
letter-spacing: 1px;
    width: 100%;
@endif ">
@if(app()->getLocale() =='ar')
خدماتنا
@elseif(app()->getLocale() =='en')
services
@endif
</h1>
<div class="container-fluid">
<h1 style="
font-size:22px;
color:#FFF;
letter-spacing:1px;
@if(app()->getLocale() =='ar')

font-family:'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;
;
letter-spacing: 1px;
    width: 100%;
@endif ">@if(app()->getLocale() =='ar')
{{$service1->headingar}}
@elseif(app()->getLocale() =='en')
{{$service1->headingen}}
@endif</h1> 
<div class="row">

<div class="col-md-6"><img src="{{url('service',$service1->image)}}" class="im"></div>
<div class="col-md-6"><h4 style="
font-size:18px;
color:#FFF;
@if(app()->getLocale() =='ar')
letter-spacing: 1px;
    width: 100%;
font-family:'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;

@endif">@if(app()->getLocale() =='ar')
{{$service1->descriptionar}}
@elseif(app()->getLocale() =='en')
{{$service1->descriptionen}}
@endif</h4></div>


<hr>

<h1 style="
font-size:22px;letter-spacing: 1px;
    width: 100%;
color:#FFF;letter-spacing:1px;
@if(app()->getLocale() =='ar')

font-family:'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;


@endif ">@if(app()->getLocale() =='ar')
{{$service2->headingar}}
@elseif(app()->getLocale() =='en')
{{$service2->headingen}}
@endif</h1>
<div class="col-md-6"><img src="{{url('service',$service2->image)}}" class="im"></div>
<div class="col-md-6"><h4 style="
font-size:18px;letter-spacing: 1px;
    width: 100%;
color:#FFF;
@if(app()->getLocale() =='ar')

font-family:'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;
;

@endif">@if(app()->getLocale() =='ar')
{{$service2->descriptionar}}
@elseif(app()->getLocale() =='en')
{{$service2->descriptionen}}
@endif
</h4></div>

<hr>

<h1 style="
font-size:22px;letter-spacing:1px;
color:#FFF;letter-spacing: 1px;
    width: 100%;
@if(app()->getLocale() =='ar')

font-family:'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;


@endif ">@if(app()->getLocale() =='ar')
{{$service3->headingar}}
@elseif(app()->getLocale() =='en')
{{$service3->headingen}}
@endif</h1>
<div class="col-md-6"><img src="{{url('service',$service3->image)}}" class="im"></div>
<div class="col-md-6"><h4  style="
font-size:18px;letter-spacing: 1px;
    width: 100%;
color:#FFF;letter-spacing:1px;
@if(app()->getLocale() =='ar')

font-family:'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;

@endif">@if(app()->getLocale() =='ar')
{{$service3->descriptionar}}
@elseif(app()->getLocale() =='en')
{{$service3->descriptionen}}
@endif</h4></div>

</div>
</div>
</div>

</main>
</div>



@endsection
