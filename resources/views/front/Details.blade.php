@extends('front.layout.app')
@section('content')

<script>(function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create','UA-72313087-1','auto');ga('send','pageview')</script>
<div class="custom">
<div id="slider" class="slido">
@include('front.navbar')

</div>
</div>  </div>
<div id="wrapper">
<header id="header" itemscope itemtype="http://schema.org/WPHeader">
<div class="inside">
<a href="https://www.cssdesignawards.com/sites/kwer/35180/" target="_blank">
<div id="cssda-badge">
<img src="files/awards/cssda-special-kudos-white.png" width="90" height="90" alt="CSS Award" />
</div>
</a>            </div>
</header>
<div class="container ce" id="container" >
<main id="main" itemscope itemtype="http://schema.org/WebPageElement" itemprop="mainContentOfPage">
<div class="inside">
<div class="mod_article first last block" id="article-68">

</div>
</div>
</main>

  
<div class="container-fluid">
	<h1 class="ce_headline ueber first">
@if(app()->getLocale() =='ar')
تفاصيل
 
@elseif(app()->getLocale() =='en')
Details

@endif 
</h1>

	<div class="row">
		<div class="col-xs-12 col-md-6">
<img src="@if(app()->getLocale() =='ar')
{{url('projecten',$project->imageen)}}
@elseif(app()->getLocale() =='en')
{{url('projectar',$project->imagear)}}
@endif" style="width:100%; max-height:500px;"></div>
		<div class="col-xs-12 col-md-6">
			
<h3 style="  color:#FFF;font-size:18px; 
    @if(app()->getLocale() =='ar')

font-family:'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;
;

@endif 

" >   @if(app()->getLocale() =='ar')
{{$project->headingar}}
@elseif(app()->getLocale() =='en')
{{$project->headingen}}
@endif</h3>

<h4 style=" letter-spacing:1px;color:#FFF;font-size:18px; 
    @if(app()->getLocale() =='ar')

font-family:'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;
;

@endif 

"  >@if(app()->getLocale() =='ar')
{{$project->descriptionar}}
@elseif(app()->getLocale() =='en')
{{$project->descriptionen}}
@endif 
@if(app()->getLocale() =='ar')
رقم الهاتف:{{$project->number}}
@elseif(app()->getLocale() =='en')
number phone:{{$project->number}}
@endif 
</h4>


		</div>
		<div class="col-xs-12">
			{!!$project->map!!}
		</div>
	</div>
</div>
</div>



@endsection
