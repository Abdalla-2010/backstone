@extends('front.layout.app')
@section('content')

<script>(function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create','UA-72313087-1','auto');ga('send','pageview')</script>
<div class="custom">
<div id="slider" class="slido">
@include('front.navbar')

</div>
</div>  </div>
<div id="wrapper">
<header id="header" itemscope itemtype="http://schema.org/WPHeader">
<div class="inside">
<a href="https://www.cssdesignawards.com/sites/kwer/35180/" target="_blank">
<div id="cssda-badge">
<img src="files/awards/cssda-special-kudos-white.png" width="90" height="90" alt="CSS Award" />
</div>
</a>            </div>
</header>
<div class="container ce" id="container" style="">
<main id="main" itemscope itemtype="http://schema.org/WebPageElement" itemprop="mainContentOfPage">
<div class="inside">
<div class="mod_article first last block" id="article-68">
<h1 class="ce_headline ueber first" style="@if(app()->getLocale() =='ar')

font-family:'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;


@endif 
letter-spacing:1px;

color:#FFF;
font-size:20px;
"
>
@if(app()->getLocale() =='ar')

{{$wordprojects->headingar}}
@elseif(app()->getLocale() =='en')

{{$wordprojects->headingen}}

@endif 
</h1>

<div class="ce_text hometext last block"
">
<p  style="
letter-spacing:1px;
@if(app()->getLocale() =='ar')

font-family:'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;


@endif 
color:#FFF;
letter-spacing:1px
"
>@if(app()->getLocale() =='ar')

{{$wordprojects->contentar}}
@elseif(app()->getLocale() =='en')

{{$wordprojects->contenten}}

@endif </p>
</div>
</div>
</div>
</main><div class="mod_newslist proj block" style="background-color: #121212">

@foreach($projects as $project)
<div class=" layout_latest arc_4 block  featured mob " style="width:50%">
<div class="grid">
<figure class="effect-sadie">
<picture>
<a href="{{url('Details',$project->id)}}"><img src="@if(app()->getLocale() =='ar')
{{url('projecten',$project->imageen)}}
@elseif(app()->getLocale() =='en')
{{url('projectar',$project->imagear)}}
@endif" alt="Mobile First bei Haus Aichlburg © KWER" title="Mobile First bei Haus Aichlburg © KWER" itemprop="image"></a>
</picture>
<script>window.respimage&&window.respimage({elements:[document.images[document.images.length-1]]})</script>
<figcaption>
<h3><a href="{{url('Details',$project->id)}}"  style="
letter-spacing:1px;
@if(app()->getLocale() =='ar')

font-family:'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;


@endif 
color:#FFF;
" class="fontmob" title="Den Artikel lesen: Haus Aichlburg" itemprop="url">@if(app()->getLocale() =='ar')
{{$project->headingar}}
@elseif(app()->getLocale() =='en')
{{$project->headingen}}
@endif</a></h3>
<!-- <p>Konzeption & Branding, Webdesign, Fotografie</p>
<a href="work-details/haus-aichlburg.html" title="Mobile First bei Haus Aichlburg © KWER">zum Projekt</a> -->
</figcaption>
</figure>
</div>
</div>
@endforeach
</div>
{{$projects->links()}}

</div>



@endsection
