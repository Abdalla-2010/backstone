@extends('front.layout.app')
@section('content')

<script>(function(i,s,o,g,r,a,m){i.GoogleAnalyticsObject=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create','UA-72313087-1','auto');ga('send','pageview')</script>
<div class="custom">
<div id="slider" class="slido">
@include('front.navbar')

</div>
</div>  </div>
<div id="wrapper">
<header id="header" >
<div class="inside">
<a href="https://www.cssdesignawards.com/sites/kwer/35180/" target="_blank">

</a>
<div class="mod_article slider videokopf first block" id="article-7">
<div class="mod_rocksolid_slider home-slider first block">
<div data-rsts-name="Kwer" data-rsts-center="true" data-rsts-class="rsts-invert-controls" >
<h1 style="  letter-spacing: 
1px;"><span data-hover="	@if(app()->getLocale() =='ar')
{{$slogan->contentar}}
@elseif(app()->getLocale() =='en')
{{$slogan->contenten}}
@endif">	@if(app()->getLocale() =='ar')
{{$slogan->contentar}}
@elseif(app()->getLocale() =='en')
{{$slogan->contenten}}
@endif
</span></h1>
<div class="ce_text block">


<video playsinline="" muted="muted" loop="loop" autoplay="autoplay" data-rsts-background>
<source src="{{url('uploadsvideo',$video->video)}}" type="video/mp4">
</video>
</div>
</div>
<script>(function($){var slider=$('.mod_rocksolid_slider').last();slider.find('video[data-rsts-background], [data-rsts-type=video] video').each(function(){this.muted=!0;this.defaultMuted=!0;this.player=!1});slider.rstSlider({"type":"slide","direction":"x","width":"100%","height":"100vh","navType":"none","scaleMode":"crop","imagePosition":"center","centerContent":!0,"random":!1,"loop":!1,"videoAutoplay":!0,"autoplayProgress":!1,"pauseAutoplayOnHover":!1,"keyboard":!1,"captions":!1,"controls":!1,"thumbControls":!1,"combineNavItems":!0,"preloadSlides":1,"visibleAreaAlign":0.5,"gapSize":"0%"});$(function(){if(!$.fn.colorbox){return}
var lightboxConfig={loop:!1,rel:function(){return $(this).attr('data-lightbox')},maxWidth:'95%',maxHeight:'95%'};var update=function(links){links.colorbox(lightboxConfig)};slider.on('rsts-slidestop',function(event){update(slider.find('a[data-lightbox]'))});update(slider.find('a[data-lightbox]'))})})(jQuery)</script>

</div>
<div class="mod_article slider mobilekopf last block" id="article-40">
<div class="mod_rocksolid_slider first block">
<div data-rsts-name="Kwer" data-rsts-center="true" data-rsts-class="rsts-invert-controls">
<h1><span data-hover="Bereit für Kreativ-Kultur?">Bereit für<br>Kreativ-Kultur?</span></h1>
<div class="ce_text block">

</div>

 
<video  muted="muted" loop="loop" autoplay="autoplay" data-rsts-background>
<source src="silder.mp4" type="video/mp4">
<source src="{{asset('silder.ogg')}}" type="video/ogg" />

</video> 
</div>
</div>
<script>(function($){var slider=$('.mod_rocksolid_slider').last();slider.find('video[data-rsts-background], [data-rsts-type=video] video').each(function(){this.muted=!0;this.defaultMuted=!0;this.player=!1});slider.rstSlider({"type":"slide","direction":"x","width":"100%","height":"100vh","navType":"none","scaleMode":"none","imagePosition":"center","centerContent":!0,"random":!1,"loop":!1,"videoAutoplay":!1,"autoplayProgress":!1,"pauseAutoplayOnHover":!1,"keyboard":!0,"captions":!1,"controls":!0,"thumbControls":!1,"combineNavItems":!0,"visibleAreaAlign":0.5,"gapSize":"0%"});$(function(){if(!$.fn.colorbox){return}
var lightboxConfig={loop:!1,rel:function(){return $(this).attr('data-lightbox')},maxWidth:'95%',maxHeight:'95%'};var update=function(links){links.colorbox(lightboxConfig)};slider.on('rsts-slidestop',function(event){update(slider.find('a[data-lightbox]'))});update(slider.find('a[data-lightbox]'))})})(jQuery)</script>

</div>
</div>
</header>
<button class="open-button wow animate__fadeInLeft  " data-wow-duration="2s" data-wow-delay="2s"><a trget="_blank" 	
				href="https://wa.me/201033389105">
<img src="{{asset('image/whatapp.png')}}" style=" border-radius: 50%;
    width: 60px;
    height: 56px;"></a>
</button>
<h4 class="open-button1 wow animate__fadeInRight  " style="  @if(app()->getLocale() =='ar')

font-family: 'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;

@endif" data-wow-duration="2s" data-wow-delay="2s"  >
@if(app()->getLocale() =='ar')
سجل الان
@elseif(app()->getLocale() =='en')
sign up Now!!
@endif
</h4>

<button class="open-button2 wow animate__fadeInRight  "  data-wow-duration="2s" data-wow-delay="2s" id="bu" onclick="openForm()"><img src="{{asset('assets/images/logo/Logo.png')}}" style=" border-radius: 50%;
    width: 70px;
    height: 66px;">
</button>
<div>
	
</div>

<div class="form-popup" id="myForm" style="border:1px solid #000;border-radius: 5px;">
	<div class="heade ">
    <button type="button" class="btn btn-danger"  style= "   position: absolute;
    /* text-align: right; */
    left:0px;" onclick="closeFormas()">x</button >
    <button class="btn" style="   position: absolute;
    position: absolute;
    left: 33px;
    background: #fbff00;" onclick="hid()">_</button>
    </div>

 <form action="{{url('sendrequesinformation')}}" class="form-container" name="va"  required onsubmit="return validateForm()" method="post" id="A" >
  		{{csrf_field()}}
  		<h3 id="new" class="alert alert-success" style="margin-top: 40px;">
          @if(app()->getLocale() =='ar')
          لقد تم ارسال طلبك بنجاح
          @elseif(app()->getLocale() =='en')
          Your request has been sent successfully
@endif
  	  </h3>
</h1>
<h2 style="font-family: 'Comic Sans MS', cursive, sans-serif	
	;">@if(app()->getLocale() =='ar')
سجل الان
@elseif(app()->getLocale() =='en')
sign up Now!!
@endif</h2>
    <div class="form-group">
 
<input type="text" name="name" id="a1" class="form-control" placeholder="@if(app()->getLocale() =='ar')
الاسم
@elseif(app()->getLocale() =='en')
name
@endif " id="name" required>
</div>
<div class="form-group">
<input type="Email" name="Email"   id="a2" class="form-control" placeholder="@if(app()->getLocale() =='ar')
البريد الالكتروني
@elseif(app()->getLocale() =='en')
Email
@endif " id="email" required>
    </div>
    <div class="form-group">
<input type="text" name="number"  id="a3" class="form-control"placeholder="@if(app()->getLocale() =='ar')
رقم الهاتف
@elseif(app()->getLocale() =='en')
Number Phone
@endif " id="phone" required>
        </div>
        <div class="form-group">
<input type="text" name="buyarea"  id="a4" class="form-control" placeholder="@if(app()->getLocale() =='ar')
المنطقه المراد الشراء بها
@elseif(app()->getLocale() =='en')
The region to buy
@endif " id="buy"  id="a5"	required >
</div>
<div class="form-group">
<input type="text" name="area" class="form-control" placeholder="@if(app()->getLocale() =='ar')
المساحة
@elseif(app()->getLocale() =='en')
The space required
@endif " id="area"  id="a6" required="">
        </div>
    </b>
        <div class="form-group">
<input  type="submit" value="@if(app()->getLocale() =='ar')
ارسل طلبك 
@elseif(app()->getLocale() =='en')
send Request
@endif  
" id="add" class="btn btn-success" style="width:200px;height: 40px;margin-top: 5px;  padding-top: 11px;"></div>
  </form>
</div>
<div class="custom" style="background-color: #fff">
<div class="jumbotron" style="background-color: #ffffff;
">
<div class="mod_article projekte first last block" id="article-8">
<div class="ce_text first block">
<h2 style="margin-bottom: 5px;margin-top: 68px; color:#000;     letter-spacing: 1px;  
@if(app()->getLocale() =='ar')

font-family: 'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;
@endif ">

@if(app()->getLocale() =='ar')
المشروعات السابقة
@elseif(app()->getLocale() =='en')
Previous project
@endif
</h2>
</div>
<!-- indexer::stop -->
<div class="mod_newslist proj block" >

@foreach($projects   as $project)
<div class="layout_latest arc_4 block featured even" style="width: 50%;">
<div class="grid">
<figure class="effect-sadie">
<picture>

<img src="@if(app()->getLocale() =='ar')
{{url('projecten',$project->imageen)}}
@elseif(app()->getLocale() =='en')
{{url('projectar',$project->imagear)}}
@endif" alt="BACKSTONE" title="BACKSTONE" itemprop="image">
</picture>
<script>window.respimage&&window.respimage({elements:[document.images[document.images.length-1]]})</script>
<figcaption>
<h3 style="  color:#FFF  letter-spacing: 1px;" class="menu"><a  href="{{url('Details',$project->id)}}"  title="Den Artikel lesen: Haus Aichlburg" itemprop="url" class=" fontmob" style="
letter-spacing: 1px;color:#000; 
    @if(app()->getLocale() =='ar')

font-family:'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;


@endif 

">@if(app()->getLocale() =='ar')
{{$project->headingar}}
@elseif(app()->getLocale() =='en')
{{$project->headingen}}
@endif</a></h3>
<!-- <p>Konzeption & Branding, Webdesign, Fotografie</p>
<a href="work-details/haus-aichlburg.html" title="Mobile First bei Haus Aichlburg © KWER">zum Projekt</a> -->
</figcaption>
</figure>
</div>
</div>
@endforeach
</div>
<!-- indexer::continue -->
</div>
</div>
<div class="custom" style="background-color: #FFF">
<div class="jumbotron" style="background-color: #FFF">
<div class="mod_article projekte first last block" id="article-8">
<div class="ce_text first block">
<h2 style="margin-bottom: 5px;margin-top: 68px;    color:#000;   letter-spacing: 1px;  
@if(app()->getLocale() =='ar')

font-family: 'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;
@endif ">

@if(app()->getLocale() =='ar')
المشروعات الحالية
@elseif(app()->getLocale() =='en')
Available project
@endif
</h2>
</div>
<!-- indexer::stop -->
<div class="mod_newslist proj block" >

@foreach($projectspri as $project)
<div class="layout_latest arc_4 block featured even" style="width: 50%;">
<div class="grid">
<figure class="effect-sadie">
<picture>
<!-- [if IE 9]><video style="display: none;"><![endif]
<source srcset="assets/images/0/aichlburg-iphone-all-kwer-086e0a3c.jpg" media="(max-width:767px)">
<source srcset="assets/images/a/aichlburg-iphone-all-kwer-e97c756a.jpg" media="(min-width: 768px) and (max-width: 991px)"> -->
<!--[if IE 9]></video><![endif]-->
<img src="@if(app()->getLocale() =='ar')
{{url('projecten',$project->imageen)}}
@elseif(app()->getLocale() =='en')
{{url('projectar',$project->imagear)}}
@endif" alt="Mobile First bei Haus Aichlburg © KWER" title="Mobile First bei Haus Aichlburg © KWER" itemprop="image">
</picture>
<script>window.respimage&&window.respimage({elements:[document.images[document.images.length-1]]})</script>
<figcaption>
<h3 style="  color:#FFF  letter-spacing: 1px;" class="menu"><a  href="{{url('Details',$project->id)}}"  title="Den Artikel lesen: Haus Aichlburg" itemprop="url" class=" fontmob" style="
letter-spacing: 1px;color:#FFF;
    @if(app()->getLocale() =='ar')

font-family:'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;


@endif 

">@if(app()->getLocale() =='ar')
{{$project->headingar}}
@elseif(app()->getLocale() =='en')
{{$project->headingen}}
@endif</a></h3>
<!-- <p>Konzeption & Branding, Webdesign, Fotografie</p>
<a href="work-details/haus-aichlburg.html" title="Mobile First bei Haus Aichlburg © KWER">zum Projekt</a> -->
</figcaption>
</figure>
</div>
</div>
@endforeach
</div>
<!-- indexer::continue -->
</div>
</div>
<div class="jumbotron unterproj" style="background-color: #fff">
<div class="">
<div class="mod_article first last block" id="article-10">
<h2 class="ce_headline hometext first" style="  letter-spacing: 1px;color:#000;
    @if(app()->getLocale() =='ar')

font-family:'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;


@endif 
\">
@if(app()->getLocale() =='ar')
{{$deal->headingar}}
@elseif(app()->getLocale() =='en')
{{$deal->headingen}}
@endif</h2>
<div class="ce_text hometext kwerteext  text2 block" style="    @if(app()->getLocale() =='ar')

font-family:'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;


@endif 
margin-bottom:50px;
  line-height: 2;
    font-size: 19px;
    margin: 0 auto;
    padding: 10px;  color:#000; background:#FFF;">
<p style="letter-spacing:1px;color:#000;font-size:18px; 
    @if(app()->getLocale() =='ar')

font-family:'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;


@endif 

color:#000;background:#FFF;">@if(app()->getLocale() =='ar')
{{$deal->contentar}}


@elseif(app()->getLocale() =='en')
{{$deal->contenten}}
</p>
@endif</div>

<div class="container" style=" padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto;
  margin-bottom:30px;
  ">
  
<h2 class="ce_headline hometext first" style="   letter-spacing: 1px;color:#000;
    @if(app()->getLocale() =='ar')

font-family:'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'	;


@endif 
">@if(app()->getLocale() =='ar')
خدماتنا
@elseif(app()->getLocale() =='en')
Services
@endif</h2>
  <div class="row">
  
    <div class="col-md-6">
    <img src="{{url('service',$service1->image)}}" class="im">
    </div>
    <div class="col-md-6"><h4 style="
font-size:18px;

@if(app()->getLocale() =='ar')
letter-spacing: 1px;
    width: 100%;
font-family:'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'  ;

@endif

color:#000;">@if(app()->getLocale() =='ar')
{{$service1->descriptionar}}
@elseif(app()->getLocale() =='en')
{{$service1->descriptionen}}
@endif</h4></div>
  </div>
</div>

@endsection
