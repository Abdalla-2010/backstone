
<div id="myNav" class="overlay">

<!-- Button to close the overlay navigation -->
<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

<!-- Overlay content -->
<div class="overlay-content">

<ul style="list-style:none">

<li  style="
   padding-top:18px;
;
@if(app()->getLocale() =='ar')
    font-size: 15px;

font-family: 'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'  ;
;
font-size: 13px;

@endif

padding-bottom:5px;"><a data-hover="about" href="{{url('aboutus')}}"  class="sibling first" itemprop="url"><span itemprop="name">
 @if(app()->getLocale() =='ar')

معلومات عنا

@elseif(app()->getLocale() =='en')
About us
@endif </span></a></li>

<li style="  letter-spacing: 1px;   padding-top:18px;
;
@if(app()->getLocale() =='ar')
    font-size: 15px;

font-family: 'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'  ;
;
font-size: 13px;

@endif

padding-bottom:5px;"><a href="{{url('Contact')}}" class="five sibling" itemprop="url"><span itemprop="name"> 
@if(app()->getLocale() =='ar')
اتصل بنا
  @elseif(app()->getLocale() =='en')
Contact us
@endif
</span></a></li>

<li style="    letter-spacing: 1px;   padding-top:18px;
;
@if(app()->getLocale() =='ar')
    font-size: 15px;

font-family: 'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'  ;
;
font-size: 13px;

@endif

padding-bottom:5px;"><a href="{{url('current_project')}}"   class="five sibling" itemprop="url"><span itemprop="name">
@if(app()->getLocale() =='ar')
المشروعات  الحالية
@elseif(app()->getLocale() =='en')
Available project
@endif
</span></a></li>
<li style="   letter-spacing: 1px;   padding-top:18px;
;
@if(app()->getLocale() =='ar')
    font-size: 15px;

font-family: 'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'  ;
;
font-size: 13px;

@endif


padding-bottom:5px;"><a  href="{{url('Previous_project')}}" class="five sibling" itemprop="url"><span itemprop="name">
@if(app()->getLocale() =='ar')
سابقة اعمالنا  
@elseif(app()->getLocale() =='en')
Previous project
@endif
</span></a></li>
<li style="   letter-spacing: 1px;   padding-top:18px;
;@if(app()->getLocale() =='ar')
    font-size: 15px;

font-family: 'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'  ;

font-size: 13px;

@endif

padding-bottom:5px;"><a  href="{{url('services')}}"  class="five sibling" itemprop="url"><span itemprop="name">
@if(app()->getLocale() =='ar')
خدماتنا
@elseif(app()->getLocale() =='en')
Services
@endif
</span></a></li>
</li>
@if(app()->getLocale() =='ar')
<li style="padding-top:20px;padding-bottom:15px;color:#ee202a;"><a href="locale/en">English</a>
</li>@elseif(app()->getLocale() =='en')
<li style="padding-top:20px;color:#ee202a;"><a href="locale/ar">  Arabic</a>
</li> 
@endif 
</ul> 

</div>

</div>

<div class="navbar-phone hidden-lg hidden-md" style="    z-index: 99999;
    position: absolute;background:black;    width: 100%;
    height: 55px;
">
  <div class="logo">
 <img src="{{asset('image/Logo.png')}}" style="  
">  </div>
   <div  style=" 

">      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" onclick="openNav() " data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar">___</span>
        <span class="icon-bar">___</span>
        <span class="icon-bar">___</span>
        
      </button>

    </div>
</div>
<div class="topbar" >

<div class=" topbar-content">
         
<div class="col-md-8 col-md-offset-4 co ">
                    <ul  class="
                    
                    @if(app()->getLocale() =='ar')
                    df
@elseif(app()->getLocale() =='en')
@endif
 list-unstyled">
                 
  <li  style="   padding-top:18px;
;@if(app()->getLocale() =='ar')
font-size: 22px;
font-family: 'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'  ;
font-size: 20px;
@endif

padding-bottom:5px;">
<a data-hover="about" href="{{url('aboutus')}}" title="Heart headed – selected to achieve more" class="sibling first" itemprop="url"><span itemprop="name">
 @if(app()->getLocale() =='ar')

معلومات عنا

@elseif(app()->getLocale() =='en')
About us
@endif </span></a></li>
<li style="   letter-spacing: 1px;   padding-top:18px;
;@if(app()->getLocale() =='ar')
    font-size: 22px;

font-family: 'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'  ;

font-size: 22px;

@endif

padding-bottom:5px;"><a  href="{{url('services')}}"  title="contact" class="five sibling" itemprop="url"><span itemprop="name">
@if(app()->getLocale() =='ar')
خدماتنا
@elseif(app()->getLocale() =='en')
Services
@endif
</span></a></li>
</li> 

<li style="    letter-spacing: 1px;   padding-top:18px;
;
@if(app()->getLocale() =='ar')
    font-size: 22px;

font-family: 'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'  ;
;
font-size: 20px;

@endif

padding-bottom:5px;"><a href="{{url('current_project')}}"  title="contact" class="five sibling" itemprop="url"><span itemprop="name">
@if(app()->getLocale() =='ar')
المشروعات  الحالية
@elseif(app()->getLocale() =='en')
Available project
@endif
</span></a></li>
<li style="   letter-spacing: 1px;   padding-top:18px;
;
@if(app()->getLocale() =='ar')
    font-size: 22px;

font-family: 'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'  ;
;
font-size: 20px;

@endif


padding-bottom:5px;"><a  href="{{url('Previous_project')}}"  title="contact" class="five sibling" itemprop="url"><span itemprop="name">
@if(app()->getLocale() =='ar')
المشروعات  السابقة
@elseif(app()->getLocale() =='en')
Previous project
@endif
</span></a></li>
<li style="  letter-spacing: 1px;   padding-top:18px;
;
@if(app()->getLocale() =='ar')
    font-size: 22px;

font-family: 'Amiri';
@elseif(app()->getLocale() =='en')
font-family: 'poppins'  ;
;
font-size: 20px;

@endif

padding-bottom:5px;"><a href="{{url('Contact')}}" title="contact" class="five sibling" itemprop="url"><span itemprop="name"> 
@if(app()->getLocale() =='ar')
اتصل بنا
  @elseif(app()->getLocale() =='en')
Contact us
@endif
</span></a></li>



@if(app()->getLocale() =='ar')
<li style="padding-top:20px;padding-bottom:15px;color:#ee202a;font-size: 20px;"><a href="locale/en">English</a>
</li>@elseif(app()->getLocale() =='en')
<li style="padding-top:20px;color:#FFF;font-size: 22px;"><a href="locale/ar">  Arabic</a>
</li> 
@endif 
                   
                    </ul>
                </div>
            <div class="col-md-4 co1" style="position: absolute;background: #000; z-index: 999999;">
      
      <div style="position: relative;">      <a href="{{route('/')}}">       <img src="{{asset('image/Logo.png')}}" style="  left: 66px;
    /* height: auto; */
    top: -5px;
    width: 100%;
    /* left: 10px; */
    max-width: 180px;
    position: absolute;
"></a</div>
                </div>
        
        
            </div>
          </div>
<script>
function openNav() {
  document.getElementById("myNav").style.width = "100%";
}

/* Close when someone clicks on the "x" symbol inside the overlay */
function closeNav() {
  document.getElementById("myNav").style.width = "0%";
}
</script>