@if($errors->any())
<div class="alert alert-danger">
	@foreach($errors->all() as $error)
<li>
	{{$error}}
</li>
@endforeach
</div>

@endif
@if(session('success'))
<div class="alert alert-success " style="position: absolute;    width: 100%;
    text-align: center;">
	{{session('success')}}
</div>

@endif
@if(session('error'))
<div class="alert alert-danger" style="position: absolute;    width: 100%;
    text-align: center;">
	{{session('error')}}
</div>	

@endif
