<div class="sidebar">
    <nav class="sidebar-nav ps ps--active-y">

        <ul class="nav">
            <li class="nav-item">
                <a href="{{ route("admin.home") }}" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt">

                    </i>
                    {{ trans('global.dashboard') }}
                </a>
            </li>
                    <li class="nav-item">
                <a href="{{url('admin/all')}}" class="nav-link ">

About                </a>
            </li>
            <li class="nav-item">
                <a href="{{url('admin/service')}}" class="nav-link ">

                service                </a>
            </li>
 <li class="nav-item">
                <a href="{{url('admin/showRequest')}}" class="nav-link ">
                  <i class="fas fa-address-card"></i>
REQUEST                </a>
            </li>
            <li class="nav-item">
                <a href="{{url('admin/contactus')}}" class="nav-link ">
                  <i class="fas fa-address-card"></i>
                  contactus                </a>
            </li>
             <li class="nav-item">
                <a href="{{url('admin/Details')}}" class="nav-link ">
                  <i class="fas fa-address-card"></i>
Details                </a>
            </li>
            <li class="nav-item">
                <a href="{{url('admin/setting')}}" class="nav-link ">
                  <i class="fas fa-address-card"></i>
setting                </a>
            </li>
            <li class="nav-item">
                <a href="{{url('admin/video')}}" class="nav-link ">
                  <i class="fas fa-address-card"></i>
video                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link" onclick="event.preventDefault(); document.getElementById('logoutform').submit();">
                    <i class="nav-icon fas fa-sign-out-alt">

                    </i>
                    {{ trans('global.logout') }}
                </a>
            </li>
        </ul>

        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
        </div>
        <div class="ps__rail-y" style="top: 0px; height: 869px; right: 0px;">
            <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 415px;"></div>
        </div>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>