@extends('layouts.admin')
@section('content')
<div class="content">
    <div class="row">
        <div class="col-lg-12">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

    <script src="https://cube.paysky.io:6006/js/LightBox.js?v=1.1"></script>

    <!-- <script src="https://grey.paysky.io:9006/invchost/JS/LightBox.js?v=1.1"></script> -->

    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <div id="Error" class="alert alert-danger" style="display: none">
        must enter
        <strong>Merchant Id</strong> and
        <strong>Terminal Id</strong>
    </div>


    <div class="card">

        <div class="card-header">

            </div>

      
    <!--  0 = Admin  -->
  
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h5 class="time2">Time Zone of America (New York)</h5>
                        <br>
                        <div class="timezone" id="timezone"></div>

                    </div>
                </div>

                <div class="row">

                    <div class="col-md-4">

                        <div class="dbox dbox--color-1 dbox1">

                            <div class="dbox__icon">

                                <i class="fa fa-users" aria-hidden="true"></i>

                            </div>

                            <div class="dbox__body">

                                <span class="dbox__count">{{$request}}</span>

                                <span class="dbox__title">Number people</span>

                            </div>


                        </div>

                    </div>

                    <div class="col-md-4">

                        <div class="dbox dbox--color-2">

                            <div class="dbox__icon">

                                <i class="fa fa-users" aria-hidden="true"></i>

                            </div>

                            <div class="dbox__body">

                                <span class="dbox__count">{{$project}}</span>

                                <span class="dbox__title">Number of available projects </span>

                            </div>


                        </div>

                    </div>

                    <div class="col-md-4">

                        <div class="dbox dbox--color-3">

                            <div class="dbox__icon">

                                <i class="fa fa-thumb-tack" aria-hidden="true"></i>

                            </div>

                            <div class="dbox__body">

                                <span class="dbox__count">{{$projects}}</span>

                                <span class="dbox__title">Number of Previous projects</span>

                            </div>


                        </div>

                    </div>

                </div>

            </div>

        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent

@endsection