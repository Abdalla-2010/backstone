@extends('layouts.admin')
@section('title','Request Information')
@section('content')
<div class="card"  style="margin-top:50px">
    <div class="card-header">
Rquest    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                <tr>
        <th>ID</th>
        <th>name</th>
        <th>Email</th>
        <th>number phone</th>
        <th>The region to buy</th>
        <th>The space required</th>
      </tr>
   
                   </thead>
                <tbody>
                @foreach($request as $re)
           <tr>
    <td>{{$re->id}}</td>
    <td>{{$re->name}}</td>
    <td>{{$re->Email}}</td>
    <td>{{$re->number}}</td>
    <td>{{$re->buyarea}}</td>
    <td>{{$re->area}}</td>

  </tr>

    @endforeach 
                </tbody>
            </table>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let deleteButtonTrans = 'DELETE'
  let deleteButton = {
  
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('product_delete')
  dtButtons.push(deleteButton)
@endcan

  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection


@endsection