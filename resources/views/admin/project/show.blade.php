@extends('layouts.admin')
@section('title','Details projects')

@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('global.product.title') }}
    </div>

    <div class="card-body">
        <table class="table table-bordered table-striped">
            <tbody>
                <tr>
                    <th>
            HeadingEnglish
                    </th>
                    <td>
                        {{ $projects->headingen }}
                        

                        
                  


                    </td>
                </tr>
                <tr>
                    <th>
HeadingArabic
                    </th>
                    <td>
                        {{ $projects->headingar }}
                    </td>
                </tr>
                <tr>
                    <th>
                        DescriptionEnglish
                    </th>
                    <td>
                        {{ $projects->descriptionen }}
                    </td>
                </tr>
                     <tr>
                    <th>
                        Description Arabic 
                    </th>
                    <td>
                          {{ $projects->descriptionar }}
                    </td>
                </tr>
                     <tr>
                    <th>
 imageen
                    </th>
                    <td>
                         <img src="{{url('projecten',$projects->imageen)}}" width="50" height="50">
                    </td>
                </tr>
                     <tr>
                    <th>
                         imagear
                    </th>
                    <td>
                                   <img src="{{url('projectar',$projects->imagear)}}" width="50" height="50">
              
                    </td>
                </tr>
                     <tr>
                    <th>
                        number
                    </th>
                    <td>
                          {{ $projects->number ?? '' }}
                    </td>
                </tr>
                     <tr>
                    <th>
                        map
                    </th>
                    <td> 
                        {!!$projects->map!!}
                    </td>
                </tr>
                     <tr>
                    <th>
                        meta_description
                    </th>
                    <td>
                        {{$projects->meta_description}}
                    </td>
                </tr>
                     <tr>
                    <th>
                        meta_keywords
                    </th>
                    <td>
                        {{$projects->meta_keywords}}
                    </td>
                </tr>
                   
            </tbody>
        </table>
    </div>
</div>

@endsection