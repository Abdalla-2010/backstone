@extends('layouts.admin')
@section('title','Edit peoject')

@section('content')


<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('global.userManagement.title_singular') }}
    </div>

    <div class="card-body">
  <div class="row">
    <form action="{{url('admin/edite',$project->id)}}" method="POST" role="form"  enctype="multipart/form-data">
            @csrf
            <div class="col-md-6">
  <div class="form-group"style="" >
        <label>HeadingEnglish</label>

   <input type="text" name="headingen" value="{{$project->headingen}}" class="form-control" >
  </div>      
            </div>
      <div class="col-md-6">
  <div class="form-group" style="" >
            <label>HeadingArabic</label>
             <input type="text" name="headingar" value="{{$project->headingar}}" class="form-control" >  
 
  </div>
      </div> 
          <div class="col-md-12">
  <div class="form-group" style="" >      
        <label>descriptionen</label>
        <textarea name="descriptionen" class="form-control">{{$project->descriptionen}}</textarea>
</div>    </div>  
<div class="col-md-12">
  <div class="form-group" style="" >
          <label>descriptionar</label>
          <textarea name="descriptionar" class="form-control">{{$project->descriptionar}}</textarea>
    </div>      
</div>

       <div class="col-md-6">
  <div class="form-group"style="" >
        <label>ImageEnglish</label>

   <input type="file" name="imageen" class="form-control">
  </div>      
            </div>
      <div class="col-md-6">
  <div class="form-group" style="" >
            <label>ImageArabic</label>
             <input type="file" name="imagear" class="form-control" >  
 
  </div>
      </div> 
         <div class="col-md-6">
  <div class="form-group" style="" >
            <label>number</label>
             <input type="text" name="number" value="{{$project->number}}" class="form-control" >  
 
  </div>
      </div> 
      <div class="col-md-6">
  <div class="form-group" style="" required>
            <label>Type Of Project</label>
            <select name="type"  class="form-control"   style="    width: 100%;
">
            <option value="1">Previous</option>
            <option value="2">Currently</option>
            </select>
 
  </div>
      </div> 
<div class="col-md-12">
  <div class="form-group" style="" >
          <label>map</label>
          <textarea name="map" class="form-control">{{$project->map}}</textarea>
    </div>      
</div>
<div class="col-md-6">
  <div class="form-group" >
          <label>meta_description</label>
          <textarea name="meta_description" class="form-control">{{$project->meta_description}}</textarea>
    </div>      
</div>

<div class="col-md-6">
  <div class="form-group" style="" >
          <label>meta_keywords</label>
          <textarea name="meta_keywords" class="form-control">{{$project->meta_keywords}}</textarea>
    </div>      
</div>
  <div class="form-group" style="" >
     <input type="submit" class="form-control btn btn-info"  value="save">
   </div>
 
    </form>

    </div>
    </div>
</div> 

@endsection