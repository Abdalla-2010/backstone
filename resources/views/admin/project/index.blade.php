@extends('layouts.admin')
@section('title','project')
@section('content')

@can('product_create')
    <div style="margin-bottom: 10px;" class="row"  style="margin-top:50px">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ url('admin/create') }}">
Add Project
            </a>
        </div>
    </div>
@endcan
<div class="card"  style="margin-top:50px">
    <div class="card-header">
        {{ trans('global.product.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th>
headingen                        
</th>          
                           <th>
                        descriptionen
                        </th>
                        
                    
                        <th>
                            imageen
                        </th>
                         
                          <th>
                            Type
                        </th>
                         
                        
                         
                        
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($n as $key => $product)
                        <tr data-entry-id="{{ $product->id }}">
                           
                            <td>
                                {{ $product->headingen ?? '' }}
                            </td>
                                  

                            <td>
                                {{ $product->descriptionen ?? '' }}
                            </td>
                                  
                                  <td>
                                <img src="{{url('projecten',$product->imageen)}}" width="50" height="50">
                               
                            </td>     
                              <td>
                                @if( $product->type  == 2)
                                Previous
                                @else
                                Currently
                                @Endif

                            </td>     

                                
                            
                            <td>
                                @can('product_show')
                                    <a class="btn btn-xs btn-primary" href="{{ url('admin/showDetails',$product->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan
                                @can('product_edit')
                                    <a class="btn btn-xs btn-info" href="{{ url('admin/editproject', $product->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
                                @can('product_delete')
                                    <form action="{{ url('admin/delete', $product->id) }}" method="POST">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let deleteButtonTrans = 'DELETE'
  let deleteButton = {
  
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('product_delete')
  dtButtons.push(deleteButton)
@endcan

  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection