@extends('layouts.admin')
@section('content')


<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('global.userManagement.title_singular') }}
    </div>

    <div class="card-body">
  <div class="row">
    <form action="{{url('admin/store')}}" method="POST" role="form"  enctype="multipart/form-data">
            @csrf
            <div class="col-md-6">
  <div class="form-group"style="" >
        <label>HeadingEnglish</label>

   <input type="text" name="headingen" class="form-control" required>
  </div>      
            </div>
      <div class="col-md-6">
  <div class="form-group" style="" >
            <label>HeadingArabic</label>
             <input type="text" name="headingar" class="form-control" required >  
 
  </div>
      </div> 
          <div class="col-md-12">
  <div class="form-group" style="" >      
        <label>descriptionen</label>
        <textarea name="descriptionen" class="form-control" required></textarea>
</div>    </div>  
<div class="col-md-12">
  <div class="form-group" style="" >
          <label>descriptionar</label>
          <textarea name="descriptionar" class="form-control" required></textarea>
    </div>      
</div>

       <div class="col-md-4">
  <div class="form-group"style="" >
        <label>ImageEnglish</label>

   <input type="file" name="imageen" class="form-control" required>
  </div>      
            </div>
      <div class="col-md-4">
  <div class="form-group" style="" >
            <label>ImageArabic</label>
             <input type="file" name="imagear" class="form-control" required>  
 
  </div>
      </div> 
         <div class="col-md-4">
  <div class="form-group" style="" >
            <label>number</label>
             <input type="text" name="number" class="form-control" required>  
 
  </div>
      </div> 

<div class="col-md-12">
  <div class="form-group" style="" >
          <label>map</label>
          <textarea name="map" class="form-control" ></textarea>
    </div>      
</div>
<div class="col-md-6">
  <div class="form-group" >
          <label>meta_description</label>
          <textarea name="meta_description" class="form-control" required></textarea>
    </div>      
</div>

<div class="col-md-6">
  <div class="form-group" style="" >
          <label>meta_keywords</label>
          <textarea name="meta_keywords" class="form-control " required></textarea>
    </div>      
</div>
  <div class="form-group" style="" >
     <input type="submit" class="form-control btn btn-info"  value="save">
   </div>
 
    </form>

    </div>
    </div>
</div> 

@endsection