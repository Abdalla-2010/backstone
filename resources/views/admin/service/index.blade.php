@extends('layouts.admin')
@section('title','serivce')
@section('content')

<div class="card"  style="margin-top:50px">
    <div class="card-header">
        {{ trans('global.product.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable">
                <thead>
                    <tr>
                        <th>
headingEnglish                       
</th>    
<th>
headingArabic                        
</th>          
              
                           <th>
                        descriptionEnglish
                        </th>
                        
                        <th>
                        descriptionArabic
                        </th>
                        <th>
                            image
                        </th>
                         
                        
                         
                        
                         
                        
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($service as $key => $services)
                        <tr data-entry-id="{{ $services->id }}">
                           
                            <td>
                                {{ $services->headingen ?? '' }}
                            </td>
                            <td>
                                {{ $services->headingar ?? '' }}
                            </td>

                            <td>
                                {{ $services->descriptionen ?? '' }}
                            </td>
                            <td>
                                {{ $services->descriptionar ?? '' }}
                            </td>
                                  
                                  <td>
                                <img src="{{url('service',$services->image)}}" width="50" height="50">
                               
                            </td>     
                            
                                
                            
                            <td>
                              
                                @can('product_edit')
                                    <a class="btn btn-xs btn-info" href="{{ url('admin/service_edit', $services->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan
                                @can('product_delete')
                                   
                                @endcan
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@section('scripts')
@parent
<script>
    $(function () {
  let deleteButtonTrans = 'DELETE'
  let deleteButton = {
  
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('product_delete')
  dtButtons.push(deleteButton)
@endcan

  $('.datatable:not(.ajaxTable)').DataTable({ buttons: dtButtons })
})

</script>
@endsection
@endsection