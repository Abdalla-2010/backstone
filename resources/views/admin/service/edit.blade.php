@extends('layouts.admin')

@section('title','Edit serivce')
@section('content')


<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('global.userManagement.title_singular') }}
    </div>

    <div class="card-body">
  <div class="row">
    <form action="{{url('admin/service_save',$service->id)}}" method="POST" role="form"  enctype="multipart/form-data">
            @csrf
            <div class="col-md-6">
  <div class="form-group"style="" >
        <label>HeadingEnglish</label>

   <input type="text" name="headingen" value="{{$service->headingen}}" class="form-control" >
  </div>      
            </div>
      <div class="col-md-6">
  <div class="form-group" style="" >
            <label>HeadingArabic</label>
             <input type="text" name="headingar" value="{{$service->headingar}}" class="form-control" >  
 
  </div>
      </div> 
          <div class="col-md-12">
  <div class="form-group" style="" >      
        <label>descriptionen</label>
        <textarea name="descriptionen" class="form-control">{{$service->descriptionen}}</textarea>
</div>    </div>  
<div class="col-md-12">
  <div class="form-group" style="" >
          <label>descriptionar</label>
          <textarea name="descriptionar" class="form-control">{{$service->descriptionar}}</textarea>
    </div>      
</div>

       <div class="col-md-12">
  <div class="form-group"style="" >
        <label>Image</label>

   <input type="file" name="image" class="form-control">
  </div>      
            </div>
  <div class="form-group" style="" >
     <input type="submit" class="form-control btn btn-info"  value="save">
   </div>
 
    </form>

    </div>
    </div>
</div> 

@endsection