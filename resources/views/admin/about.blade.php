@extends('layouts.admin')
@section('title','About')
@section('content')

<div class="card">
    <div class="card-header">
About information
    </div>

    <div class="card-body">
        <form action="{{ url('admin/aboutsto',$about->id) }}" method="POST">
            @csrf
              <div class="form-group" >
        <label>HeadingEnglish</label>

   <input type="text" name="headingen" class="form-control" value="{{$about->headingen}}" required>
  </div>      
  <div class="form-group"  >
            <label>HeadingArabic</label>
             <input type="text" name="headingar" class="form-control" value="{{$about->headingar}}" required>  
  </div>      
  <div class="form-group"  >      
        <label>contentEnglish</label>
        <textarea name="contenten" class="form-control" required>{{$about->contenten}}</textarea>
</div>      
  <div class="form-group"  >
          <label>contentArabic</label>
          <textarea name="contentar" class="form-control" required>{{$about->contentar}}</textarea>
    </div>      
  <div class="form-group" >
     <input type="submit" class="form-control btn btn-info"  value="save">
   </div>
 
    </form>
    </div>
    </div>
</div>
</div>

@endsection